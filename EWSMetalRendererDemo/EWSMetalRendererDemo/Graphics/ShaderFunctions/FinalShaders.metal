//
//  FinalShaders.metal
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

#include <metal_stdlib>
#include "SharedStructures.metal"
using namespace metal;

vertex FinalRasterizerData final_vertex_shader(const VertexIn vertexIn [[ stage_in ]]) {
    FinalRasterizerData finalRasterizerData;
    
    finalRasterizerData.finalPosition = float4(vertexIn.postion, 1.0);
    finalRasterizerData.finalTextureCoordinate = vertexIn.textureCoordinate;
    
    return finalRasterizerData;
};

fragment half4 final_fragment_shader(const FinalRasterizerData finalRasterizerData [[ stage_in ]],
                                     sampler sampler2D [[ sampler(0) ]],
                                     texture2d<float> finalTexture) {
    
    float2 finalTextureCoordinate = finalRasterizerData.finalTextureCoordinate;
    
    // Invert render view axis
    finalTextureCoordinate.y = 1 - finalTextureCoordinate.y;
    
    float4 finalColor = finalTexture.sample(sampler2D, finalTextureCoordinate);
    
    return half4(finalColor);
}

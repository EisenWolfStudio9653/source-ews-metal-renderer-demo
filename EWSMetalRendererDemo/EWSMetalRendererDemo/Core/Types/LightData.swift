//
//  LightData.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation
import simd

struct LightData: Sizeable {
    var position: SIMD3<Float> = SIMD3<Float>(repeating: 0.0)
    var color: SIMD3<Float> = SIMD3<Float>(repeating: 1.0)
    var brightness: Float = 1.0
    var ambientIntensity: Float = 1.0
    var diffuseIntensity: Float = 1.0
    var specularIntensity: Float = 1.0
}

//
//  TextureTypes.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

enum TextureTypes {
    case void

    case baseColorRender // For colorAttachment[0]
    case customColor1Render // For colorAttachment[1]
    case customColor2Render // For colorAttachment[2]
    case customColor3Render // For colorAttachment[3]
    case customColor4Render // For colorAttachment[4]
    case customColor5Render // For colorAttachment[5]
    
    case baseDepthRender // For depthAttachment
    
    // Metal Plate
    case diffuseMetalPlate
    case normalMetalPlate
    
    // Sky Sphere
    case skySphereCloud
}

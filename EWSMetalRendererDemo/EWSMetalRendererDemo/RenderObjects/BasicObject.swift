//
//  BasicObject.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class BasicObject: Node {
    private var renderPipelineStateType = RenderPipelineStateTypes.basic
    
    private var modelConstants = ModelConstants()
    private var mesh: Mesh?
    
    // Texture
    private var baseColorTextureType: TextureTypes = TextureTypes.void
    private var normalMapTextureType: TextureTypes = TextureTypes.void
    
    // Material
    private var material: Material? = nil
    
    init(name: String, meshType: MeshTypes) {
        debugPrint("<EWS> BasicObject -> init => <\(name)>")
        
        self.mesh = Assets.manager.getMeshLibrary()[meshType]
        
        super.init(name: name)
    }
    
    // Override Node
    override func performUpdate() {
        self.modelConstants.modelMatrix = self.getModelMatrix()
        
        super.performUpdate()
    }
    
}

extension BasicObject {
    final public func setBaseColorTexture(textureType: TextureTypes) {
        self.baseColorTextureType = textureType
    }
    
    final public func setNormalMapTexture(textureType: TextureTypes) {
        self.normalMapTextureType = textureType
    }
    
    final public func setMaterial(material: Material) {
        self.material = material
    }
    
    final public func setRenderPipelineStateType(type: RenderPipelineStateTypes) {
        self.renderPipelineStateType = type
    }
    
}

extension BasicObject: Renderable {
    func executeRenderable(with renderCommandEncoder: MTLRenderCommandEncoder, shadowRenderpass: Bool) {
        // Outline
//        renderCommandEncoder.setTriangleFillMode(MTLTriangleFillMode.lines)
        // Fill
//        renderCommandEncoder.setTriangleFillMode(MTLTriangleFillMode.fill)
        
        renderCommandEncoder.setRenderPipelineState(Graphics.manager.getRenderPipelineStateLibrary()[self.renderPipelineStateType])
        renderCommandEncoder.setDepthStencilState(Graphics.manager.getDepthStencilStateLibrary()[DepthStencilStateTypes.less])
        
        // Vertex Shader
        renderCommandEncoder.setVertexBytes(
            &self.modelConstants,
            length: ModelConstants.stride,
            index: 2
        )
        
        // Fragment Shader
        
        
        // Relocate setFragmentTexture in Submesh
        
//        renderCommandEncoder.setFragmentSamplerState(
//            Graphics.manager.getSamplerStateLibrary()[SamplerStateTypes.linear],
//            index: 0
//        )
        
//        renderCommandEncoder.setFragmentBytes(
//            &self.material,
//            length: Material.stride,
//            index: 1
//        )
        
//        if self.material.useTextureColor == true {
//            renderCommandEncoder.setFragmentTexture(
//                Entities.manager.getTextureLibrary()[self.textureType],
//                index: 0
//            )
//        }
        
        
        self.mesh?.drawPrimitives(
            renderCommandEncoder: renderCommandEncoder,
            baseColorTextureType: self.baseColorTextureType,
            normalMapTextureType: self.normalMapTextureType,
            material: self.material,
            shadowRenderpass: shadowRenderpass
        )
    }
}

//
//  ShadowShader.metal
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

#include <metal_stdlib>
#include "SharedStructures.metal"
using namespace metal;

struct ShadowVertexIn {
    float3 postion [[ attribute(0) ]];
    float4 color [[ attribute(1) ]];
    float2 textureCoordinate [[ attribute(2) ]];
};

struct ShadowRasterizerData {
    float4 position [[ position ]];
    float2 textureCoordinate;
    
    float3 worldPosition;
    float3 cameraVector;
};

vertex float4 shadow_vertex_shader(const ShadowVertexIn shadowVertexIn [[ stage_in ]], 
                                   constant SceneConstants &sceneConstants [[ buffer(0) ]],
                                   constant ModelConstants &modelConstants [[ buffer(1) ]]) {

    ShadowRasterizerData shadowRasterizerData;
    
    float4 worldPosition = modelConstants.modelMatrix * float4(shadowVertexIn.postion, 1.0);
    
    shadowRasterizerData.position = sceneConstants.viewMatrix * sceneConstants.projectionMatrix * worldPosition;
    
    return float4(0);
};



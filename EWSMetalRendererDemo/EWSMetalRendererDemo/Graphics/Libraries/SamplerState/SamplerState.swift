//
//  SamplerState.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

protocol SamplerState {
    var name: String { get }
    var state: MTLSamplerState! { get }
}

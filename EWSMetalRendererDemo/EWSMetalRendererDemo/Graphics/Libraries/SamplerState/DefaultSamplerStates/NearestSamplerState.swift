//
//  NearestSamplerState.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

struct NearestSamplerState: SamplerState {
    var name: String = "NearestSamplerState"
    var state: MTLSamplerState!
    
    init() {
        let samplerDescriptor = MTLSamplerDescriptor()
        
        samplerDescriptor.label = self.name
        
        samplerDescriptor.minFilter = MTLSamplerMinMagFilter.nearest
        samplerDescriptor.magFilter = MTLSamplerMinMagFilter.nearest
        samplerDescriptor.mipFilter = MTLSamplerMipFilter.nearest
        
        self.state = MetalEngine.shared.getMetalDevice().makeSamplerState(descriptor: samplerDescriptor)
    }
}

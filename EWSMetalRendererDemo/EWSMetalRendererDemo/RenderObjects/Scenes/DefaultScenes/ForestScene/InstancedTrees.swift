//
//  InstancedTrees.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

class InstancedTrees: Node {
    init() {
        super.init(name: "InstancedTrees")
        
        let treesPineA = InstancedObject(
            name: "treesPineA",
            meshType: MeshTypes.natureTreePineA,
            instantceCount: 2500
        )
        treesPineA.updateNodes(nodePositionFunction: self.updateTreePosition)
        self.addChild(child: treesPineA)
        
        let treesPineB = InstancedObject(
            name: "treesPineB",
            meshType: MeshTypes.natureTreePineB,
            instantceCount: 2500
        )
        treesPineB.updateNodes(nodePositionFunction: self.updateTreePosition)
        self.addChild(child: treesPineB)
        
        let treesPineC = InstancedObject(
            name: "treesPineC",
            meshType: MeshTypes.natureTreePineC,
            instantceCount: 2500
        )
        treesPineC.updateNodes(nodePositionFunction: self.updateTreePosition)
        self.addChild(child: treesPineC)
        
    }
    
}

extension InstancedTrees {
    private func updateTreePosition(tree: Node, index: Int) {
        let treeRadius: Float = Float.random(in: 3...300)
        tree.setPosition(
            x: cos(Float(index)) * treeRadius,
            y: 0.0,
            z: sin(Float(index)) * treeRadius
        )
        tree.setScale(scaleFactor: Float.random(in: 1...2))
        tree.rotate(deltaY: Float.random(in: 0...360))
    }
}

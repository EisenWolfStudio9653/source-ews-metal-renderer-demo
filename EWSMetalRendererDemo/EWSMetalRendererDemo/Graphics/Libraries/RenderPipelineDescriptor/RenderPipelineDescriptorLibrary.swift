//
//  RenderPipelineDescriptorLibrary.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class RenderPipelineDescriptorLibrary: Library<RenderPipelineDescriptorTypes, MTLRenderPipelineDescriptor> {
    private var libraryCache = [RenderPipelineDescriptorTypes: RenderPipelineDescriptor]()
    
    override subscript(type: RenderPipelineDescriptorTypes) -> MTLRenderPipelineDescriptor {
        let metalRenderPipelineDescriptor = self.libraryCache[type]!.descriptor!
        
        return metalRenderPipelineDescriptor
    }
    
    override func performSetupDefaultLibrary() {
        self.libraryCache.updateValue(BasicRenderPipelineDescriptor(), forKey: RenderPipelineDescriptorTypes.basic)
        
        self.libraryCache.updateValue(InstancedRenderPipelineDescriptor(), forKey: RenderPipelineDescriptorTypes.instanced)
    }

}

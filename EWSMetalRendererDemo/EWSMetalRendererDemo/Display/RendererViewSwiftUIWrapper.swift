//
//  RendererViewSwiftUIWrapper.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import SwiftUI

struct RendererViewSwiftUIWrapper: NSViewRepresentable {
    
    func makeNSView(context: Context) -> some NSView {
        return RendererView(frame: CGRect(
            origin: CGPoint.zero,
            size: CGSize(width: 1024, height: 768)
        ))
    }
    
    func updateNSView(_ nsView: NSViewType, context: Context) {
        
    }
}

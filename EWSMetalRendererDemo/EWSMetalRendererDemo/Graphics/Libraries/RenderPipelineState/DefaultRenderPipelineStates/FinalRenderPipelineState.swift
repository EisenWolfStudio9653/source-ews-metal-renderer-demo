//
//  FinalRenderPipelineState.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

struct FinalRenderPipelineState: RenderPipelineState {
    var name: String = "FinalRenderPipelineState"
    
    var state: MTLRenderPipelineState!
    
    init() {
        let metalRenderPipelineDescriptor = MTLRenderPipelineDescriptor()
        metalRenderPipelineDescriptor.label = "FinalRenderPipelineDescriptor"
        
        metalRenderPipelineDescriptor.colorAttachments[0].pixelFormat = EnginePreference.shared.getPixelFormat(for: PixelFormatTypes.mainColor)
        
        metalRenderPipelineDescriptor.vertexDescriptor = Graphics.manager.getVertexDescriptorLibrary()[VertexDescriptorTypes.basic]
        
        metalRenderPipelineDescriptor.vertexFunction = Graphics.manager.getVertexShaderLibrary()[VertexShaderTypes.final]
        metalRenderPipelineDescriptor.fragmentFunction = Graphics.manager.getFragmentShaderLibrary()[FragmentShaderTypes.final]
        
        
        do {
            self.state = try MetalEngine.shared.getMetalDevice().makeRenderPipelineState(descriptor: metalRenderPipelineDescriptor)
        } catch {
            debugPrint("<EWS> FinalRenderPipelineState Error")
        }
    }
}

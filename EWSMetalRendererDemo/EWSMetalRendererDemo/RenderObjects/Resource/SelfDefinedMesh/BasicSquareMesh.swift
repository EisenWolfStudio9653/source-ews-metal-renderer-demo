//
//  BasicSquareMesh.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

class BasicSquareMesh: Mesh {
    override func setupMesh() {
        
        // Top Right
        self.addVertex(
            position: SIMD3<Float>(1.0, 1.0, 0.0),
            color: SIMD4<Float>(0.0, 0.0, 1.0, 1.0),
            textureCoordinate: SIMD2<Float>(1.0, 1.0),
            normal: SIMD3<Float>(0.0, 1.0, 0.0)
        )
        
        // Top Left
        self.addVertex(
            position: SIMD3<Float>(-1.0, 1.0, 0.0),
            color: SIMD4<Float>(0.0, 1.0, 0.0, 1.0),
            textureCoordinate: SIMD2<Float>(0.0, 1.0),
            normal: SIMD3<Float>(0.0, 1.0, 0.0)
        )
        
        // Bottom Left
        self.addVertex(
            position: SIMD3<Float>(-1.0, -1.0, 0.0),
            color: SIMD4<Float>(1.0, 0.0, 0.0, 1),
            textureCoordinate: SIMD2<Float>(0.0, 0.0),
            normal: SIMD3<Float>(0.0, 1.0, 0.0)
        )
        
        // Top Right
//        self.addVertex(
//            position: SIMD3<Float>(1.0, 1.0, 0.0),
//            color: SIMD4<Float>(0.0, 0.0, 1.0, 1.0),
//            textureCoordinate: SIMD2<Float>(1.0, 1.0),
//            normal: SIMD3<Float>(0.0, 1.0, 0.0)
//        )
        
        // Bottom Left
//        self.addVertex(
//            position: SIMD3<Float>(-1.0, -1.0, 0.0),
//            color: SIMD4<Float>(1.0, 0.0, 0.0, 1),
//            textureCoordinate: SIMD2<Float>(0.0, 0.0),
//            normal: SIMD3<Float>(0.0, 1.0, 0.0)
//        )
        
        // Bottom Right
        self.addVertex(
            position: SIMD3<Float>(1.0, -1.0, 0.0),
            color: SIMD4<Float>(0.0, 1.0, 0.0, 1.0),
            textureCoordinate: SIMD2<Float>(1.0, 0.0),
            normal: SIMD3<Float>(0.0, 1.0, 0.0)
        )
        
        // Indicies
        self.addSubmesh(
            submesh: Submesh(
                indices: [
                    0, 1, 2,
                    0, 2, 3
                ]
            )
        )
        
    }
    
}

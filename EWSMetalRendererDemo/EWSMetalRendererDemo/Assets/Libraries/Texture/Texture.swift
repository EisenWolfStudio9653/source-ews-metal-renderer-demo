//
//  Texture.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

enum TextureOrigin {
    case topLeft
    case bottomleft
}

class Texture {
    private var texture: MTLTexture?
    
    init(metalTexture: MTLTexture) {
        self.texture = metalTexture
    }
    
    init(textureName: String, textureExtension: String = "png", origin: TextureOrigin) {
        
        // Load from bundle
        guard let resourceURL = Bundle.main.url(forResource: textureName, withExtension: textureExtension)
        else {
            debugPrint("<EWS> Texture -> init => resourceURL Error")
            return
        }
        
        var textureOrigin: MTKTextureLoader.Origin
        switch origin {
        case TextureOrigin.bottomleft:
            textureOrigin = MTKTextureLoader.Origin.bottomLeft
        case TextureOrigin.topLeft:
            textureOrigin = MTKTextureLoader.Origin.topLeft
        }
        
        let textureLoader = MTKTextureLoader(device: MetalEngine.shared.getMetalDevice())
        let loaderOptions: [MTKTextureLoader.Option : Any] = [
            MTKTextureLoader.Option.origin: textureOrigin,
            MTKTextureLoader.Option.generateMipmaps: true
        ]
        
        do {
            self.texture = try textureLoader.newTexture(URL: resourceURL, options: loaderOptions)
            self.texture?.label = textureName
        } catch {
            debugPrint("<EWS> Texture -> init => textureLoader.newTexture Error")
        }
    
    }
}

extension Texture {
    public func setTexture(texture: MTLTexture) {
        self.texture = texture
    }
    
    public func getTexture() -> MTLTexture? {
        return self.texture
    }
}

//
//  SamplerStateTypes.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

enum SamplerStateTypes {
    case none
    case linear
    case nearest
}

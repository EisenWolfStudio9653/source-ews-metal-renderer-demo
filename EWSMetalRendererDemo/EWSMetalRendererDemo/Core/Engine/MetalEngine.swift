//
//  MetalEngine.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

final class MetalEngine {
    private init() {}
    public static let shared = MetalEngine()
    
    private var sharedDevice: MTLDevice!
    private var sharedCommandQueue: MTLCommandQueue!
    private var sharedLibrary: MTLLibrary!
    
}

extension MetalEngine {
    public func ignite(metalDevice: MTLDevice) {
        self.sharedDevice = metalDevice
        self.sharedCommandQueue = metalDevice.makeCommandQueue()
        self.sharedLibrary = metalDevice.makeDefaultLibrary()
        
        Graphics.manager.initialize()
        Assets.manager.initialize()
        
    }
    
    public func getMetalDevice() -> MTLDevice {
        return self.sharedDevice
    }
    
    public func getMetalCommandQueue() -> MTLCommandQueue {
        return self.sharedCommandQueue
    }
    
    public func getMetalLibrary() -> MTLLibrary {
        return self.sharedLibrary
    }
}

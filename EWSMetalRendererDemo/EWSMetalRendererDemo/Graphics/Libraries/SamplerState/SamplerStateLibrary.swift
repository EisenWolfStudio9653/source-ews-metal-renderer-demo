//
//  SamplerStateLibrary.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class SamplerStateLibrary: Library<SamplerStateTypes, MTLSamplerState> {
    private var libraryCache = [SamplerStateTypes: SamplerState]()
    
    override func performSetupDefaultLibrary() {
        self.libraryCache.updateValue(LinearSamplerState(), forKey: SamplerStateTypes.linear)
        self.libraryCache.updateValue(NearestSamplerState(), forKey: SamplerStateTypes.nearest)
    }
    
    override subscript(type: SamplerStateTypes) -> MTLSamplerState? {
        let metalSamplerState = self.libraryCache[type]!.state
        
        return metalSamplerState
    }
}

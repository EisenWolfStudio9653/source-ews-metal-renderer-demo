//
//  LightObject.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class LightObject: BasicObject {
    private var lightData = LightData()
    
    init(name: String) {
        super.init(name: name, meshType: MeshTypes.void)
    }
    
    override init(name: String, meshType: MeshTypes) {
        super.init(name: name, meshType: meshType)
    }
    
    // Override Node
    override func performUpdate() {
        self.lightData.position = SIMD3<Float>(
            self.getPositionX(),
            self.getPositionY(),
            self.getPositionZ()
        )
        
        super.performUpdate()
    }

}

extension LightObject {
    public func getLightData() -> LightData {
        return self.lightData
    }
    
    // Light Color
    public func setLightColor(r: Float, g: Float, b: Float) {
        self.lightData.color = SIMD3<Float>(r, g, b)
    }
    
    public func getLightColorR() -> Float {
        return self.lightData.color.x
    }
    
    public func getLightColorG() -> Float {
        return self.lightData.color.y
    }
    
    public func getLightColorB() -> Float {
        return self.lightData.color.z
    }
    
    // Light Brightness
    public func setLightBrightness(brightness: Float) {
        self.lightData.brightness = brightness
    }
    
    public func getLightBrightness() -> Float {
        return self.lightData.brightness
    }
    
    // Ambient Intensity
    public func setLightAmbientIntensity(intensity: Float) {
        self.lightData.ambientIntensity = intensity
    }
    
    public func getLightAmbientIntensity() -> Float {
        return self.lightData.ambientIntensity
    }
    
    // Diffuse Intensity
    public func setLightDiffuseIntensity(intensity: Float) {
        self.lightData.diffuseIntensity = intensity
    }
    
    public func getLightDifuseIntensity() -> Float {
        return self.lightData.diffuseIntensity
    }
    
    // Specular Intensity
    public func setLightSpecularIntensity(intensity: Float) {
        self.lightData.specularIntensity = intensity
    }
    
    public func getLightSpecularIntensity() -> Float {
        return self.lightData.specularIntensity
    }
    
}

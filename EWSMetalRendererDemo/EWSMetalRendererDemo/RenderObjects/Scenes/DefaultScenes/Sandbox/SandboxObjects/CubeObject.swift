//
//  CubeObject.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

class CubeObject: BasicObject {
    private var camera: Camera
    
    init(camera: Camera) {
        self.camera = camera
        
        super.init(name: "CubeObject", meshType: MeshTypes.basicCube)
    }
    
    override func performPreUpdate() {
        let deltaTime = EngineTime.shared.getDeltaTime()
        
        self.rotate(deltaX: deltaTime)
        self.rotate(deltaY: deltaTime)
        
    }
    
}

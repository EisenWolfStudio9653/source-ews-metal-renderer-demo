//
//  InstancedRenderPipelineState.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

struct InstancedRenderPipelineState: RenderPipelineState {
    var name: String = "InstancedRenderPipelineState"
    
    var state: MTLRenderPipelineState!
    
    init() {
        let metalRenderPipelineDescriptor = MTLRenderPipelineDescriptor()
        
        metalRenderPipelineDescriptor.colorAttachments[0].pixelFormat = EnginePreference.shared.getPixelFormat(for: PixelFormatTypes.mainColor)
        metalRenderPipelineDescriptor.colorAttachments[1].pixelFormat = EnginePreference.shared.getPixelFormat(for: PixelFormatTypes.mainColor)
        metalRenderPipelineDescriptor.colorAttachments[2].pixelFormat = EnginePreference.shared.getPixelFormat(for: PixelFormatTypes.mainColor)
        
        metalRenderPipelineDescriptor.depthAttachmentPixelFormat = EnginePreference.shared.getPixelFormat(for: PixelFormatTypes.mainDepth)
        
        metalRenderPipelineDescriptor.vertexDescriptor = Graphics.manager.getVertexDescriptorLibrary()[VertexDescriptorTypes.basic]
        
        metalRenderPipelineDescriptor.vertexFunction = Graphics.manager.getVertexShaderLibrary()[VertexShaderTypes.instanced]
        metalRenderPipelineDescriptor.fragmentFunction = Graphics.manager.getFragmentShaderLibrary()[FragmentShaderTypes.instanced]
        
        do {
            self.state = try MetalEngine.shared.getMetalDevice().makeRenderPipelineState(descriptor: metalRenderPipelineDescriptor)
        } catch {
            debugPrint("<EWS> InstancedRenderPipelineState Error")
        }
    }
}

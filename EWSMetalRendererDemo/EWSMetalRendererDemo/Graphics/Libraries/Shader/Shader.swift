//
//  Shader.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class Shader {
    private var function: MTLFunction?
    
    init(name: String, functionName: String) {
        self.function = MetalEngine.shared.getMetalLibrary().makeFunction(name: functionName)
        self.function?.label = name
    }
}

extension Shader {
    public func getFunction() -> MTLFunction? {
        return self.function
    }
}

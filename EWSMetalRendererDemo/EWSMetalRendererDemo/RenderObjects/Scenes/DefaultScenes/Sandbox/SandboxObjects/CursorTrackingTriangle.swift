//
//  CursorTrackingTriangle.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

class CursorTrackingTriangle: BasicObject {
    private var camera: Camera
    
    init(camera: Camera) {
        self.camera = camera
        
        super.init(name: "BasicTriangle", meshType: MeshTypes.basicTriangle)
    }
   
    override func performPreUpdate() {

        let x = CursorInputs.manager.getRelativeCursorPosition().x - self.getPositionX() + self.camera.getPositionX()
        let y = CursorInputs.manager.getRelativeCursorPosition().y - self.getPositionY() + self.camera.getPositionY()
        
        self.rotate(deltaZ: -atan2f(x, y))
        
    }
    
}

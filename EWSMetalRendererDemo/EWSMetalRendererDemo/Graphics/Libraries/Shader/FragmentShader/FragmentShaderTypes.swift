//
//  FragmentShaderTypes.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

enum FragmentShaderTypes {
    case basic
    case instanced
    case final
    case skySphere
}

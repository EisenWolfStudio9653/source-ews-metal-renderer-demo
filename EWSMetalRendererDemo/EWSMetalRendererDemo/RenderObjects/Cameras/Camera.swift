//
//  Camera.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation
import simd

class Camera: Node {
    private var viewMatrix = matrix_identity_float4x4
    private var projectionMatrix = matrix_identity_float4x4
    
    private let cameraType: CameraTypes
    
    init(name: String, cameraType: CameraTypes) {
        self.cameraType = cameraType
 
        super.init(name: name)
    }
    
    override func performModelMatrixTransformation() {
        self.viewMatrix = matrix_identity_float4x4
        
        // - MARK: matrix rotation before traslation
        
        
        // Rotation
        self.viewMatrix.rotate(
            angle: self.getRotationX(),
            axis: Math.shared.axisX
        )
        
        self.viewMatrix.rotate(
            angle: self.getRotationY(),
            axis: Math.shared.axisY
        )
        
        self.viewMatrix.rotate(
            angle: self.getRotationZ(),
            axis: Math.shared.axisZ
        )
        
        // Translation
        self.viewMatrix.translate(
            direction: -SIMD3<Float>(
                self.getPositionX(),
                self.getPositionY(),
                self.getPositionZ()
            )
        )
        
    }
}



extension Camera {
    final public func getCameraType() -> CameraTypes {
        return self.cameraType
    }
    
    final public func getViewMatrix() -> matrix_float4x4 {
        return self.viewMatrix
    }
    
    final public func setProjectionMatrix(matrix: matrix_float4x4) {
        self.projectionMatrix = matrix
    }
    
    final public func getProjectionMatrix() -> matrix_float4x4 {
        return self.projectionMatrix
    }
}

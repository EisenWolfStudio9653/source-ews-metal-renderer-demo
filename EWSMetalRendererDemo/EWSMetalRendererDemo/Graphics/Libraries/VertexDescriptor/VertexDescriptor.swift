//
//  VertexDescriptor.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

protocol VertexDescriptor {
    var name: String { get }
    var descriptor: MTLVertexDescriptor! { get }
}

//
//  SceneTypes.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

enum SceneTypes {
    case sandbox
    case forest
    case type2
    case type3
}

//
//  SceneConstants.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation
import simd

struct SceneConstants: Sizeable {
    var totalEngineTime: Float = 0.0
    var skySphereViewMatrix = matrix_identity_float4x4
    var viewMatrix = matrix_identity_float4x4
    var projectionMatric = matrix_identity_float4x4
    var cameraPosition: SIMD3<Float> = SIMD3<Float>(repeating: 0.0)
}

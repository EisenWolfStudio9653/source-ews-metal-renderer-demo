//
//  Renderable.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

protocol Renderable {
    func executeRenderable(with renderCommandEncoder: MTLRenderCommandEncoder, shadowRenderpass: Bool)
    
}

//
//  EngineTime.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

final class EngineTime {
    private init() {}
    public static let shared = EngineTime()
    
    private var totalTime: Float = 0.0
    private var deltaTime: Float = 0.0
    
}

extension EngineTime {
    public func update(time: Float) {
        self.deltaTime = time
        self.totalTime += time
    }
    
    public func getTotalTime() -> Float {
        return self.totalTime
    }
    
    public func getDeltaTime() -> Float {
        return self.deltaTime
    }
}

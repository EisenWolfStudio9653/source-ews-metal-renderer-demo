//
//  Model_Corgi.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

class Model_Corgi: BasicObject {
    init() {
        super.init(name: "Model_Corgi", meshType: MeshTypes.model_Corgi)
        
        self.setScale(scaleFactor: 0.015)
        
        self.setRotation(
            x: Float(90.0).toDegree,
            z: Float(90.0).toDegree
        )
    }
    
    override func performPreUpdate() {
       
        
    }
}

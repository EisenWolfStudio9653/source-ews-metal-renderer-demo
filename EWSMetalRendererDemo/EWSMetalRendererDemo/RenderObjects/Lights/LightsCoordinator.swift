//
//  LightsCoordinator.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class LightsCoordinator {
    private var lightsContainer = [LightObject]()
    
}

extension LightsCoordinator {
    private func getLightDataContainer() -> [LightData] {
        var result = [LightData]()
        
        for lightObject in self.lightsContainer {
            let lightData = lightObject.getLightData()
            result.append(lightData)
        }
        
        return result
    }
}

extension LightsCoordinator {
    public func addLight(lightObject: LightObject) {
        self.lightsContainer.append(lightObject)
    }
    
    public func setLightData(renderCommandEncoder: MTLRenderCommandEncoder) {
        var lightDataContainer = self.getLightDataContainer()
        var lightDataContainerCount = lightDataContainer.count
        
        renderCommandEncoder.setFragmentBytes(
            &lightDataContainerCount,
            length: Int32.size,
            index: 2
        )
        
        renderCommandEncoder.setFragmentBytes(
            &lightDataContainer,
            length: LightData.stride(lightDataContainerCount),
            index: 3
        )
    }
}

//
//  Node.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class Node {
    private var name: String
    private let id: String

    private var position = SIMD3<Float>(repeating: 0)
    private var scale = SIMD3<Float>(repeating: 1)
    private var rotation = SIMD3<Float>(repeating: 0)
    
    private var parentModelMatrix = matrix_identity_float4x4
    private var modelMatrix = matrix_identity_float4x4
    
    private var children = [Node]()
    
    init(name: String) {
        self.name = name
        self.id = UUID().uuidString
    }
    
    // Base functions for overriding
    public func performPreUpdate() {}
    
    public func performUpdate() {
        self.performPreUpdate()
        
        for child in self.children {
            child.parentModelMatrix = self.modelMatrix
            child.performUpdate()
        }
    }
    
    public func performPostTranslaion() {}
    
    public func performPostRotation() {}
    
    public func performPostScaling() {}
    
    public func performRender(renderCommandEncoder: MTLRenderCommandEncoder) {}
    
    public func performModelMatrixTransformation() {
        self.modelMatrix = matrix_identity_float4x4
        
        // Translation
        self.modelMatrix.translate(direction: self.position)
        
        // Rotation
        self.modelMatrix.rotate(angle: self.rotation.x, axis: Math.shared.axisX)
        self.modelMatrix.rotate(angle: self.rotation.y, axis: Math.shared.axisY)
        self.modelMatrix.rotate(angle: self.rotation.z, axis: Math.shared.axisZ)
        
        // Scale
        self.modelMatrix.scale(axis: self.scale)
    }
    
}

extension Node {
    private func updateModelMatrixTranslation(value: SIMD3<Float>) {
        self.position = value
        self.performModelMatrixTransformation()
        self.performPostTranslaion()
    }
    
    private func updateModelMatrixRotation(value: SIMD3<Float>) {
        self.rotation = value
        self.performModelMatrixTransformation()
        self.performPostRotation()
    }
    
    private func updateModelMatrixScaling(value: SIMD3<Float>) {
        self.scale = value
        self.performModelMatrixTransformation()
        self.performPostScaling()
    }
}

extension Node {
    
    final public func render(renderCommandEncoder: MTLRenderCommandEncoder, shadowRenderpass: Bool) {
        self.performRender(renderCommandEncoder: renderCommandEncoder)
        
        /* ======================== GPU DebugGroup ==========================*/
        renderCommandEncoder.pushDebugGroup("<EWS> Rendering Node <\(self.name)>")
        
        if let renderable = self as? Renderable {
            renderable.executeRenderable(with: renderCommandEncoder, shadowRenderpass: shadowRenderpass)
        }
        
        // Recursively iterate through each entry and their sub-entries
        for child in self.children {
            child.render(renderCommandEncoder: renderCommandEncoder, shadowRenderpass: shadowRenderpass)
        }
        
        renderCommandEncoder.popDebugGroup()
        /* ======================== GPU DebugGroup ==========================*/
    }
    
    final public func addChild(child: Node) {
        self.children.append(child)
    }
    
    final public func getChildren() -> [Node] {
        return self.children
    }
    
    final public func getModelMatrix() -> matrix_float4x4 {
        return matrix_multiply(self.parentModelMatrix, self.modelMatrix)
    }
    
    // Identification    
    final public func getName() -> String {
        return self.name
    }
    
    final public func getID() -> String {
        return self.id
    }
    
    // Translation
    final public func setPosition(x: Float? = nil, y: Float? = nil, z: Float? = nil) {
        var updatedValue = SIMD3<Float>(self.position.x, self.position.y, self.position.z)
        
        if let x = x {
            updatedValue.x = x
        }
        
        if let y = y {
            updatedValue.y = y
        }
        
        if let z = z {
            updatedValue.z = z
        }
        
        self.updateModelMatrixTranslation(value: updatedValue)
    }
    
    final public func move(deltaX: Float? = nil, deltaY: Float? = nil, deltaZ: Float? = nil) {
        var updatedValue = SIMD3<Float>(self.position.x, self.position.y, self.position.z)
        
        if let deltaX = deltaX {
            updatedValue.x += deltaX
        }
        
        if let deltaY = deltaY {
            updatedValue.y += deltaY
        }
        
        if let deltaZ = deltaZ {
            updatedValue.z += deltaZ
        }
        
        self.updateModelMatrixTranslation(value: updatedValue)
    }
    
    final public func getPositionX() -> Float {
        return self.position.x
    }
    
    final public func getPositionY() -> Float {
        return self.position.y
    }
    
    final public func getPositionZ() -> Float {
        return self.position.z
    }
    
    // Rotation
    final public func setRotation(x: Float? = nil, y: Float? = nil, z: Float? = nil) {
        var updatedValue = SIMD3<Float>(self.rotation.x, self.rotation.y, self.rotation.z)
        
        if let x = x {
            updatedValue.x = x
        }
        
        if let y = y {
            updatedValue.y = y
        }
        
        if let z = z {
            updatedValue.z = z
        }
        
        self.updateModelMatrixRotation(value: updatedValue)
    }
    
    final public func rotate(deltaX: Float? = nil, deltaY: Float? = nil, deltaZ: Float? = nil) {
        var updatedValue = SIMD3<Float>(self.rotation.x, self.rotation.y, self.rotation.z)
        
        if let deltaX = deltaX {
            updatedValue.x += deltaX
        }
        
        if let deltaY = deltaY {
            updatedValue.y += deltaY
        }
        
        if let deltaZ = deltaZ {
            updatedValue.z += deltaZ
        }
        
        self.updateModelMatrixRotation(value: updatedValue)
    }
    
    final public func getRotationX() -> Float {
        return self.rotation.x
    }
    
    final public func getRotationY() -> Float {
        return self.rotation.y
    }
    
    final public func getRotationZ() -> Float {
        return self.rotation.z
    }
    
    // Scaling
    final public func setScale(x: Float? = nil, y: Float? = nil, z: Float? = nil) {
        var updatedValue = SIMD3<Float>(self.scale.x, self.scale.y, self.scale.z)
        
        if let x = x {
            updatedValue.x = x
        }
        
        if let y = y {
            updatedValue.y = y
        }
        
        if let z = z {
            updatedValue.z = z
        }
        
        self.updateModelMatrixScaling(value: updatedValue)
    }
    
    final public func setScale(scaleFactor: Float) {
        let updatedValue = SIMD3<Float>(repeating: scaleFactor)
        
        self.updateModelMatrixScaling(value: updatedValue)
    }
    
    final public func scale(deltaX: Float? = nil, deltaY: Float? = nil, deltaZ: Float? = nil) {
        var updatedValue = SIMD3<Float>(self.scale.x, self.scale.y, self.scale.z)
        
        if let deltaX = deltaX {
            updatedValue.x += deltaX
        }
        
        if let deltaY = deltaY {
            updatedValue.y += deltaY
        }
        
        if let deltaZ = deltaZ {
            updatedValue.z += deltaZ
        }
        
        self.updateModelMatrixScaling(value: updatedValue)
    }
    
    final public func getScaleX() -> Float {
        return self.scale.x
    }
    
    final public func getScaleY() -> Float {
        return self.scale.y
    }
    
    final public func getScaleZ() -> Float {
        return self.scale.z
    }
}

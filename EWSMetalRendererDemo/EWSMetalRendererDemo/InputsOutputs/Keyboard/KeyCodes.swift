//
//  KeyCodes.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

// US-ANSI Keyboard
enum KeyCodes: UInt16 {
    // Modifier Keys
    case escapeKey = 0x35
    case spaceKey =  0x31
    case returnKey = 0x24
    case deleteKey = 0x33
    case tabKey = 0x30
    case graveKey = 0x32
    case capsLockKey = 0x39
    case homeKey = 0x73
    case endKey = 0x77
    case pageUpKey = 0x74
    case pageDown = 0x79
    case commandKey = 0x37
    case shiftKey = 0x38
    case optionKey = 0x3A
    case controlKey = 0x3B
    case rightCommand = 0x36
    case rightShiftKey = 0x3C
    case rightOptionKey = 0x3D
    case rightControl = 0x3E
    
    // Function Keys
    case functionKey = 0x3F
    case f1Key = 0x7A
    case f2Key = 0x78
    case f3Key = 0x63
    case f4Key = 0x76
    case f5Key = 0x60
    case f6Key = 0x61
    case f7Key = 0x62
    case f8Key = 0x64
    case f9Key = 0x65
    case f10Key = 0x6D
    case f11Key = 0x67
    case f12Key = 0x6F
    case f13Key = 0x69
    case f14Key = 0x6B
    case f15Key = 0x71
    
    // Arrow Keys
    case upArrowKey = 0x7E
    case downArrowKey = 0x7D
    case leftArrowKey = 0x7B
    case rightArrowKey = 0x7C
    
    // Alphabet Keys
    case aKey = 0x00
    case bKey = 0x0B
    case cKey = 0x08
    case dKey = 0x02
    case eKey = 0x0E
    case fKey = 0x03
    case gKey = 0x05
    case hKey = 0x04
    case iKey = 0x22
    case jKey = 0x26
    case kKey = 0x28
    case lKey = 0x25
    case mKey = 0x2E
    case nKey = 0x2D
    case oKey = 0x1F
    case pKey = 0x23
    case qKey = 0x0C
    case rKey = 0x0F
    case sKey = 0x01
    case tKey = 0x11
    case uKey = 0x20
    case vKey = 0x09
    case wKey = 0x0D
    case xKey = 0x07
    case yKey = 0x10
    case zKey = 0x06
    
    // Number Keys
    case number0Key = 0x1D
    case number1Key = 0x12
    case number2Key = 0x13
    case number3Key = 0x14
    case number4Key = 0x15
    case number5Key = 0x17
    case number6Key = 0x16
    case number7Key = 0x1A
    case number8Key = 0x1C
    case number9Key = 0x19
    
    // Special Characters
    case minusKey = 0x1B
    case plusKey = 0x18
    case commaKey = 0x2B
    case periodKey = 0x2F
    case semicolonKey = 0x29
    case forwardSlashKey = 0x2C
    case backslashKey = 0x2A
    case leftBracketKey = 0x21
    case rightBracketKey = 0x1E
    
}

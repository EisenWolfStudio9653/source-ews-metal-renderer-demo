//
//  BasicShader.metal
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

#include <metal_stdlib>
#include "SharedStructures.metal"
using namespace metal;



vertex RasterizerData basic_vertex_shader(const VertexIn vertexIn [[ stage_in ]],
                                          constant SceneConstants &sceneConstants [[ buffer(1) ]],
                                          constant ModelConstants &modelConstants [[ buffer(2) ]]) {
    RasterizerData rasterizerData;
    
    rasterizerData.totalEngineTime = sceneConstants.totalEngineTime;
    
    float4 worldPosition = modelConstants.modelMatrix * float4(vertexIn.postion, 1.0);
    
    rasterizerData.position = sceneConstants.projectionMatrix * sceneConstants.viewMatrix * worldPosition;
    rasterizerData.color = vertexIn.color;
    rasterizerData.textureCoordinate = vertexIn.textureCoordinate;
    
    rasterizerData.worldPosition = worldPosition.xyz;
    rasterizerData.cameraVector = sceneConstants.cameraPosition - worldPosition.xyz;
    
    rasterizerData.surfaceNormal = normalize(modelConstants.modelMatrix * float4(vertexIn.normal, 0.0)).xyz;
    rasterizerData.surfaceTangent = normalize(modelConstants.modelMatrix * float4(vertexIn.tangent, 0.0)).xyz;
    rasterizerData.surfaceBitangent = normalize(modelConstants.modelMatrix * float4(vertexIn.bitangent, 0.0)).xyz;
    
    return rasterizerData;
}

fragment FragmentOutput basic_fragment_shader(RasterizerData rasterizerData [[ stage_in ]],
                                     constant Material &material [[ buffer(1) ]],
                                     constant int &lightDataContainerCount [[ buffer(2) ]],
                                     constant LightData *lightDataContainer [[ buffer(3) ]],
                                     sampler sampler2D [[ sampler(0) ]],
                                     texture2d<float> baseColorMap [[ texture(0) ]],
                                     texture2d<float> normalMap [[ texture(1) ]]) {

    float4 color = material.color;
    
    if (material.enableBaseColorTexture == true) {
        color = baseColorMap.sample(sampler2D, rasterizerData.textureCoordinate);
    }
 
    if (material.enableLights == true) {
        float3 unitNormal = normalize(rasterizerData.surfaceNormal);
        
        if (material.enableNormalMapTexture == true) {
            // UnitNormal * 2 - 1, Convert from range (-1, 1) to (0, 1)
            float3 sampleNormal = normalMap.sample(sampler2D, rasterizerData.textureCoordinate).rgb * 2 - 1;
            
            // Construct TBN matrix
            float3x3 matrixTBN = {
                rasterizerData.surfaceTangent,
                rasterizerData.surfaceBitangent,
                rasterizerData.surfaceNormal
            };
            
            unitNormal = matrixTBN * sampleNormal;
        }
        
        // - MARK: ShadowMapping
//        for (int index = 0; index < lightDataContainerCount; index++) {
//            LightData lightData = lightDataContainer[index];
//            float4 lightSpacePosition =
//        }
        
        
        // - MARK: PhongShading
        float3 unitCameraVector = normalize(rasterizerData.cameraVector);
        
        float3 totalAmbient = float3(0, 0, 0);
        float3 totalDiffuse = float3(0, 0, 0);
        float3 totalSpecular = float3(0, 0, 0);
        
        for (int index = 0; index < lightDataContainerCount; index++) {
            LightData lightData = lightDataContainer[index];
            
            // Ambient Lighting
            float3 ambientness = material.ambient * lightData.ambientIntensity;
            
            float3 ambientColor = clamp(ambientness * lightData.color * lightData.brightness, 0.0, 1.0);
            
            totalAmbient += ambientColor;
            
            // Diffuse Lighting
            float3 diffuseness = material.diffuse * lightData.diffuseIntensity;
            
            float3 unitLightVector = normalize(lightData.position - rasterizerData.worldPosition);
            float dotProductNL = max(dot(unitNormal, unitLightVector), 0.0);
            
            float3 diffuseColor = clamp(diffuseness * dotProductNL * lightData.color * lightData.brightness, 0.0, 1.0);
            
            totalDiffuse += diffuseColor;
            
            // Specular Lighting
            float3 specularness = material.specular * lightData.specularIntensity;
            
            float3 unitReflectionVector = normalize(reflect(-(unitLightVector), unitNormal));
            float dotProductRV = max(dot(unitReflectionVector, unitCameraVector), 0.0);
            
            float specularExponent = pow(dotProductRV, material.shinines);
            
            float3 specularColor = clamp(specularness * specularExponent * lightData.color * lightData.brightness, 0.0, 1.0);
            
            totalSpecular += specularColor;
        }
        
        float3 phongIntensity = totalAmbient + totalDiffuse + totalSpecular;
        
        color *= float4(phongIntensity, 1.0);
    }
    
    FragmentOutput fragmentOutput;
    fragmentOutput.color0 = half4(color.r, color.g, color.b, color.a);
    fragmentOutput.color1 = half4(rasterizerData.surfaceNormal.x, rasterizerData.surfaceNormal.y, rasterizerData.surfaceNormal.z, 1.0);
    fragmentOutput.color2 = half4(color.r, color.g, color.b, color.a);
    
    return fragmentOutput;
}


//
//  SkySphere.metal
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

#include <metal_stdlib>
#include "../SharedStructures.metal"
using namespace metal;

vertex RasterizerData sky_sphere_vertex_shader(const VertexIn vertexIn [[ stage_in ]],
                                               constant SceneConstants &sceneConstants [[ buffer(1) ]],
                                               constant ModelConstants &modelConstants [[ buffer(2) ]]) {
    RasterizerData rasterizerData;
    rasterizerData.totalEngineTime = sceneConstants.totalEngineTime;
    
    float4 worldPosition = modelConstants.modelMatrix * float4(vertexIn.postion, 1.0);
    
    rasterizerData.position = sceneConstants.projectionMatrix * sceneConstants.skySphereViewMatrix * worldPosition;
    rasterizerData.textureCoordinate = vertexIn.textureCoordinate;
    
    return rasterizerData;
};

fragment half4 sky_sphere_fragment_shader(RasterizerData rasterizerData [[ stage_in ]], 
                                          sampler sampler2D [[ sampler(0) ]],
                                          texture2d<float> skySphereTexture [[ texture(10) ]]) {
    
    float4 color = skySphereTexture.sample(
                                           sampler2D,
                                           rasterizerData.textureCoordinate,
                                           level(0)
                                           );
    
    return half4(color.r, color.g, color.b, color.a);
}

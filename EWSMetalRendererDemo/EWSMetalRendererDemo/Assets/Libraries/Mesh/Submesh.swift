//
//  Submesh.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class Submesh {
    private var primitiveType = MTLPrimitiveType.triangle
    private var indexBuffer: MTLBuffer?
    private var indexBufferOffset: Int = 0
    private var indexType = MTLIndexType.uint32
    private var indexCount: Int = 0
    private var indicesContainer = [UInt32]()
    private var baseColorTexture: MTLTexture?
    private var normalMapTexture: MTLTexture?
    private var material = Material()
    
    init(indices: [UInt32]) {
        self.indicesContainer.removeAll()
        self.indicesContainer.append(contentsOf: indices)
        self.indexCount = self.indicesContainer.count
        
        // Index buffer
        if self.indicesContainer.count > 0 {
            self.indexBuffer = MetalEngine.shared.getMetalDevice().makeBuffer(
                bytes: self.indicesContainer,
                length: UInt32.stride(self.indicesContainer.count),
                options: []
            )
        }
        
    }
    
    init(metalKitSubmesh: MTKSubmesh, modelSubmesh: MDLSubmesh) {
        self.primitiveType = metalKitSubmesh.primitiveType
        self.indexBuffer = metalKitSubmesh.indexBuffer.buffer
        self.indexBufferOffset = metalKitSubmesh.indexBuffer.offset
        self.indexType = metalKitSubmesh.indexType
        self.indexCount = metalKitSubmesh.indexCount
        
        
        guard let modelMaterial = modelSubmesh.material else {
            debugPrint("<EWS> Submesh -> init with MTK & MDL Submesh => material nil")
            return
        }
        
        // Setup texture
        self.baseColorTexture = self.loadTexture(
            materialSemantic: MDLMaterialSemantic.baseColor,
            modelMaterial: modelMaterial, 
            textureOrigin: MTKTextureLoader.Origin.bottomLeft
        )
        
        self.normalMapTexture = self.loadTexture(
            materialSemantic: MDLMaterialSemantic.tangentSpaceNormal,
            modelMaterial: modelMaterial,
            textureOrigin: MTKTextureLoader.Origin.bottomLeft
        )
            
        // Setup Material
        if let ambient = modelMaterial.property(with: MDLMaterialSemantic.emission)?.float3Value {
            self.material.ambient = ambient
        }
        
        if let diffuse = modelMaterial.property(with: MDLMaterialSemantic.baseColor)?.float3Value {
            self.material.diffuse = diffuse
        }
        
        if let specular = modelMaterial.property(with: MDLMaterialSemantic.specular)?.float3Value {
            self.material.specular = specular
        }
        
        if let shinines = modelMaterial.property(with: MDLMaterialSemantic.specularExponent)?.floatValue {
            self.material.shinines = shinines
        }
 
    }
}

extension Submesh {
    private func loadTexture(materialSemantic: MDLMaterialSemantic, modelMaterial: MDLMaterial, textureOrigin: MTKTextureLoader.Origin) -> MTLTexture? {
        
        guard let materialProperty = modelMaterial.property(with: materialSemantic),
                let sourceTexture = materialProperty.textureSamplerValue?.texture
        else {
            debugPrint("<EWS> Submesh -> loadTexture => texture nil")
            return nil
        }
        
        let textureLoader = MTKTextureLoader(device: MetalEngine.shared.getMetalDevice())
        
        // mipmapping
        let mipmaps: Bool
        if (sourceTexture.dimensions.x > 2) && (sourceTexture.dimensions.y > 2) {
            
        }
        
        let loadedTexture = try? textureLoader.newTexture(
            texture: sourceTexture,
            options: [
                MTKTextureLoader.Option.origin: textureOrigin,
                MTKTextureLoader.Option.generateMipmaps: true
            ]
        )
        
        return loadedTexture
    }
    
}

extension Submesh {
    final public func applyTexture(renderCommandEncoder: MTLRenderCommandEncoder, samplerStateTypes: SamplerStateTypes = SamplerStateTypes.linear, baseColorTextureType: TextureTypes, normalMapTextureType: TextureTypes) {
        
        if baseColorTextureType != TextureTypes.void || self.baseColorTexture != nil {
            self.material.enableBaseColorTexture = true
        } else {
            self.material.enableBaseColorTexture = false
        }

        if normalMapTextureType != TextureTypes.void || self.normalMapTexture != nil {
            self.material.enableNormalMapTexture = true
        } else {
            self.material.enableNormalMapTexture = false
        }
        
        renderCommandEncoder.setFragmentSamplerState(
            Graphics.manager.getSamplerStateLibrary()[samplerStateTypes],
            index: 0
        )
        
        // Base color texture
        let fragmentBaseColorTexture: MTLTexture?
        if baseColorTextureType == TextureTypes.void {
            fragmentBaseColorTexture = self.baseColorTexture
        } else {
            fragmentBaseColorTexture = Assets.manager.getTextureLibrary()[baseColorTextureType]
        }
        
        if fragmentBaseColorTexture != nil {
            renderCommandEncoder.setFragmentTexture(
                fragmentBaseColorTexture,
                index: 0
            )
        }
        
        // Normal map texture
        let fragmentNormalmapTexture: MTLTexture?
        if normalMapTextureType == TextureTypes.void {
            fragmentNormalmapTexture = self.normalMapTexture
        } else {
            fragmentNormalmapTexture = Assets.manager.getTextureLibrary()[normalMapTextureType]
        }
        
        if fragmentNormalmapTexture != nil {
            renderCommandEncoder.setFragmentTexture(
                fragmentNormalmapTexture,
                index: 1
            )
        }
        
    }
    
    final public func applyMaterial(renderCommandEncoder: MTLRenderCommandEncoder, material: Material?) {
        
        var fragmentMaterial: Material?
        if material == nil {
            fragmentMaterial = self.material
        } else {
            fragmentMaterial = material
        }
        
        renderCommandEncoder.setFragmentBytes(
            &fragmentMaterial,
            length: Material.stride,
            index: 1
        )
        
    }
    
    final public func getPrimitiveType() -> MTLPrimitiveType {
        return self.primitiveType
    }
    
    final public func getIndexType() -> MTLIndexType {
        return self.indexType
    }
    
    final public func getIndexBufferOffset() -> Int {
        return self.indexBufferOffset
    }
    
    final public func getIndexBuffer() -> MTLBuffer? {
        return self.indexBuffer
    }
    
    final public func getIndexCount() -> Int {
        return self.indexCount
    }
    
}

//
//  CursorInputsManager.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation
import simd

final class CursorInputs {
    private init() {
        self.mouseButtonsActuationStateContainer = [Bool](repeating: false, count: self.mouseButtonCounts)
    }
    public static let manager = CursorInputs()
    
    private let mouseButtonCounts: Int = 12
    private var mouseButtonsActuationStateContainer: [Bool]
    
    private var overallCusorPosition = SIMD2<Float>(repeating: 0.0)
    private var deltaCursorPosition = SIMD2<Float>(repeating: 0.0)
    
    private var currentScrollPosition: Float = 0.0
    private var deltaScrollPosition: Float = 0.0
    private var previousScrollPosition: Float = 0.0
    
}

extension CursorInputs {
    // Mouse Buttons
    public func setMouseButtonPressed(mouseButton: Int, isOn: Bool) {
        self.mouseButtonsActuationStateContainer[mouseButton] = isOn
    }
    
    public func isMouseButtonPressed(mouseButtonCode: CursorButtonCodes) -> Bool {
        let state = self.mouseButtonsActuationStateContainer[Int(mouseButtonCode.rawValue)]
        
        return state
    }
    
    // Cursor Position
    public func setOverallCursorPosition(updatedPosition: SIMD2<Float>) {
        self.overallCusorPosition = updatedPosition
    }
    
    public func setCursorPositionChange(overallPosition: SIMD2<Float>, deltaPosition: SIMD2<Float>) {
        self.overallCusorPosition = overallPosition
        self.deltaCursorPosition = deltaPosition
    }
    
    public func getActualCursorPosition() -> SIMD2<Float> {
        return self.overallCusorPosition
    }
    
    public func getRelativeCursorPosition() -> SIMD2<Float> {
        let xPosition = (self.overallCusorPosition.x - RendererProperties.manager.getScreenSize().x * 0.5) / (RendererProperties.manager.getScreenSize().x * 0.5) * 10
        let yPosition = (self.overallCusorPosition.y - RendererProperties.manager.getScreenSize().y * 0.5) / (RendererProperties.manager.getScreenSize().y * 0.5) * 10
        let relativePosition = SIMD2<Float>(xPosition, yPosition)
        
        return relativePosition
    }
    
    public func getCursorDeltaXPosition() -> Float {
        let deltaX = self.deltaCursorPosition.x
        self.deltaCursorPosition.x = 0.0
        
        return deltaX
    }
    
    public func getCursorDeltaYPosition() -> Float {
        let deltaY = self.deltaCursorPosition.y
        self.deltaCursorPosition.y = 0.0
        
        return deltaY
    }
    
    // Scroll
    public func setVerticalScrollPosition(deltaY: Float) {
        self.previousScrollPosition = self.currentScrollPosition
        
        self.currentScrollPosition += deltaY
        self.deltaScrollPosition += deltaY
    }
    
    public func getDeltaScrollPosition() -> Float {
        let deltaPosition = self.deltaScrollPosition
        self.deltaScrollPosition = 0.0
        
        return deltaPosition
    }
    
}

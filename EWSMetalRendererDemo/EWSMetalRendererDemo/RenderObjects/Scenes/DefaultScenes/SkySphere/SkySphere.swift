//
//  SkySphere.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class SkySphere: BasicObject {
    private var textureType: TextureTypes
    
    init(name: String, textureType: TextureTypes) {
        
        self.textureType = textureType
        
        super.init(name: name, meshType: MeshTypes.sphere)
        
        self.setRenderPipelineStateType(type: RenderPipelineStateTypes.skySphere)
        
        self.setScale(scaleFactor: 50.0)
        
    }
    
    override func performRender(renderCommandEncoder: MTLRenderCommandEncoder) {
        // Set index 10 for sky sphere texture
        renderCommandEncoder.setFragmentTexture(Assets.manager.getTextureLibrary()[self.textureType], index: 10)
        
        super.performRender(renderCommandEncoder: renderCommandEncoder)
    }

}

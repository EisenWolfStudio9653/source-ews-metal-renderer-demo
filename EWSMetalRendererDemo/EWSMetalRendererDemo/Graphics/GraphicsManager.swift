//
//  GraphicsManager.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

final class Graphics {
    private init() {}
    public static let manager = Graphics()
    
    private var vertexShaderLibrary: VertexShaderLibrary?
    private var fragmentShaderLibrary: FragmentShaderLibrary?
    private var vertexDescriptorLibrary: VertexDescriptorLibrary?
    private var renderPipelineDescriptorLibrary: RenderPipelineDescriptorLibrary?
    private var renderPipelineStateLibrary: RenderPipelineStateLibrary?
    private var depthStencilStateLibrary: DepthStencilStateLibrary?
    private var samplerStateLibrary: SamplerStateLibrary?
    
}

extension Graphics {
    public func initialize() {
        self.vertexShaderLibrary = VertexShaderLibrary()
        self.fragmentShaderLibrary = FragmentShaderLibrary()
        self.vertexDescriptorLibrary = VertexDescriptorLibrary()
        self.renderPipelineDescriptorLibrary = RenderPipelineDescriptorLibrary()
        self.renderPipelineStateLibrary = RenderPipelineStateLibrary()
        self.depthStencilStateLibrary = DepthStencilStateLibrary()
        self.samplerStateLibrary = SamplerStateLibrary()
    }
    
    public func getVertexShaderLibrary() -> VertexShaderLibrary {
        return self.vertexShaderLibrary!
    }
    
    public func getFragmentShaderLibrary() -> FragmentShaderLibrary {
        return self.fragmentShaderLibrary!
    }
    
    public func getVertexDescriptorLibrary() -> VertexDescriptorLibrary {
        return self.vertexDescriptorLibrary!
    }
    
    public func getRenderPipelineDescriptorLibrary() -> RenderPipelineDescriptorLibrary {
        return self.renderPipelineDescriptorLibrary!
    }
    
    public func getRenderPipelineStateLibrary() -> RenderPipelineStateLibrary {
        return self.renderPipelineStateLibrary!
    }
    
    public func getDepthStencilStateLibrary() -> DepthStencilStateLibrary {
        return self.depthStencilStateLibrary!
    }
    
    public func getSamplerStateLibrary() -> SamplerStateLibrary {
        return self.samplerStateLibrary!
    }
}

//
//  FragmentShaderLibrary.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class FragmentShaderLibrary: Library<FragmentShaderTypes, MTLFunction> {
    private var libraryCache = [FragmentShaderTypes: Shader]()
    
    override subscript(type: FragmentShaderTypes) -> MTLFunction {
        let metalFunction = (self.libraryCache[type]?.getFunction())!
        
        return metalFunction
    }
    
    override func performSetupDefaultLibrary() {
        
        self.libraryCache.updateValue(
            Shader(
                name: "BasicFragmentShader",
                functionName: "basic_fragment_shader"
            ),
            forKey: FragmentShaderTypes.basic
        )
        
        self.libraryCache.updateValue(
            Shader(
                name: "InstancedFragmentShader",
                functionName: "instanced_fragment_shader"
            ),
            forKey: FragmentShaderTypes.instanced
        )
        
        self.libraryCache.updateValue(
            Shader(
                name: "FinalFragmentShader",
                functionName: "final_fragment_shader"
            ),
            forKey: FragmentShaderTypes.final
        )
        
        self.libraryCache.updateValue(
            Shader(
                name: "SkySphereFragmentShader",
                functionName: "sky_sphere_fragment_shader"
            ),
            forKey: FragmentShaderTypes.skySphere
        )
        
    }
    
}

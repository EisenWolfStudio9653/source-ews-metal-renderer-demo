//
//  Model_Chest.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

class Model_Chest: BasicObject {
    init() {
        super.init(name: "Model_Chest", meshType: MeshTypes.model_Chest)
        
        self.setScale(scaleFactor: 0.01)
    }
    
    override func performPreUpdate() {
        let deltaTime = EngineTime.shared.getDeltaTime()
        
        if CursorInputs.manager.isMouseButtonPressed(mouseButtonCode: CursorButtonCodes.leftButton) {
            self.rotate(deltaX: CursorInputs.manager.getCursorDeltaYPosition() * deltaTime)
            self.rotate(deltaY: CursorInputs.manager.getCursorDeltaXPosition() * deltaTime)
        }
        
    }
}

//
//  InstancedCubeCollection.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

class InstancedCubeCollection: InstancedObject {
    private var width: Int = 0
    private var height: Int = 0
    private var depth: Int = 0
    private var color: SIMD4<Float>
    
    init(color: SIMD4<Float>, width: Int, height: Int, depth: Int) {
        self.color = color
        
        super.init(
            name: "InstancedCubeCollection",
            meshType: MeshTypes.basicCube,
            instantceCount: width * height * depth
        )
        
        self.width = width
        self.height = height
        self.depth = depth
    
        let cubeMaterial = Material(color: SIMD4<Float>(0.5, 0.3, 0.1, 1.0))
        self.setMaterial(material: cubeMaterial)
    }
    
    override func performPreUpdate() {
        let spacingFactor: Float = cos(EngineTime.shared.getTotalTime() / 2) * 10
        var nodeIndex: Int = 0
        
        for y in stride(from: -Float(self.height / 2), to: Float(self.height / 2), by: 1.0) {
            let positionY = Float(y * spacingFactor)
            
            for x in stride(from: -Float(self.width / 2), to: Float(self.width / 2), by: 1.0) {
                let positionX = Float(x * spacingFactor)
                
                for z in stride(from: -Float(self.depth / 2), to: Float(self.depth / 2), by: 1.0) {
                    let positionZ = Float(z * spacingFactor)
                    
                    self.getNodes()[nodeIndex].setPosition(x: positionX, y: positionY, z: positionZ)
                    
                    self.getNodes()[nodeIndex].setRotation(
                        x: 1.0,
                        y: -EngineTime.shared.getDeltaTime() * 2, 
                        z: -EngineTime.shared.getDeltaTime() * 2
                    )
                    
                    self.getNodes()[nodeIndex].setScale(scaleFactor: 0.3)
                    
                    nodeIndex += 1
                }
            }
        }
        
    }
    
}

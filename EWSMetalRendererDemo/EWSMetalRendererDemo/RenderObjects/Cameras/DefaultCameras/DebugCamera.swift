//
//  DebugCamera.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation
import simd

class DebugCamera: Camera {
    private var debugCameraProjectionMatrix = matrix_identity_float4x4
    
    private var moveSpeed: Float = 3.0
    private var turnSpeed: Float = 1.0

    init() {
        super.init(name: "DebugCamera", cameraType: CameraTypes.debug)
         
        // - TODO: Refactor
        self.debugCameraProjectionMatrix.perspectiveProjection(
            degreesFOV: 45.0,
            aspectRatio: RendererProperties.manager.getAspectRatio(),
            near: 0.1,
            far: 1000.0
        )
        self.setProjectionMatrix(matrix: self.debugCameraProjectionMatrix)
        // End of refector
        
    }
    
    // Override Node
    override func performPreUpdate() {
        
        let deltaTime = EngineTime.shared.getDeltaTime()
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.upArrowKey) == true {
            self.move(deltaY: deltaTime * self.moveSpeed * 5)
        }
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.downArrowKey) == true {
            self.move(deltaY: -(deltaTime) * self.moveSpeed * 5)
        }
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.leftArrowKey) == true {
            self.move(deltaX: -(deltaTime) * self.moveSpeed)
        }
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.rightArrowKey) == true {
            self.move(deltaX: deltaTime * self.moveSpeed)
        }
        
        
        
        if CursorInputs.manager.isMouseButtonPressed(mouseButtonCode: CursorButtonCodes.middleButton) == true {
            self.move(
                deltaX: -(CursorInputs.manager.getCursorDeltaXPosition()) * deltaTime * self.moveSpeed,
                deltaY: CursorInputs.manager.getCursorDeltaYPosition() * deltaTime * self.moveSpeed
            )
        }
        
        
        // Horozontal Movement
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.wKey) == true {
            self.move(deltaZ: -(deltaTime) * self.moveSpeed)
        }
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.aKey) == true {
            self.move(deltaX: -(deltaTime) * self.moveSpeed)
        }
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.sKey) == true {
            self.move(deltaZ: deltaTime * self.moveSpeed)
        }
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.dKey) == true {
            self.move(deltaX: deltaTime * self.moveSpeed)
        }
        
        // Camera View Direction
        self.rotate(
            deltaX: CursorInputs.manager.getCursorDeltaYPosition() * deltaTime * self.turnSpeed,
            deltaY: CursorInputs.manager.getCursorDeltaXPosition() * deltaTime * self.turnSpeed,
            deltaZ: 0.0
        )
        
//        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.cKey) == true {
//            self.rotate(
//                deltaX: CursorInputs.manager.getCursorDeltaYPosition() * deltaTime * self.turnSpeed,
//                deltaY: CursorInputs.manager.getCursorDeltaXPosition() * deltaTime * self.turnSpeed,
//                deltaZ: 0.0
//            )
//        }

    }
    
}

extension DebugCamera {
 
}

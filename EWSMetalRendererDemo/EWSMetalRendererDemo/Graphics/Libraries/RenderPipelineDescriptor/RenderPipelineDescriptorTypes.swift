//
//  RenderPipelineDescriptorTypes.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

enum RenderPipelineDescriptorTypes {
    case basic
    case instanced
}

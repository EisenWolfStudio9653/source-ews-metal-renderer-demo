//
//  BaseScene.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class BaseScene: Node {
    private var sceneConstants = SceneConstants()
    
    private var lightsCoordinator = LightsCoordinator()
    private var camerasCoordinator = CamerasCoordinator()
    
    override init(name: String) {
        super.init(name: name)
        
        self.buildScene()
    }
    
    // Base functions for overriding
    public func buildScene() {}
    
    // Override Node
    override func performUpdate() {
        self.sceneConstants.totalEngineTime = EngineTime.shared.getTotalTime()
        
        self.sceneConstants.viewMatrix = camerasCoordinator.getCurrentCamera().getViewMatrix()
        
        self.sceneConstants.projectionMatric = camerasCoordinator.getCurrentCamera().getProjectionMatrix()
        
        // Sky sphere matrix = view matrix removing translation.
        self.sceneConstants.skySphereViewMatrix = self.sceneConstants.viewMatrix
        self.sceneConstants.skySphereViewMatrix[3][0] = 0
        self.sceneConstants.skySphereViewMatrix[3][1] = 0
        self.sceneConstants.skySphereViewMatrix[3][2] = 0

        
        let currentCameraPosition = SIMD3<Float>(
            self.camerasCoordinator.getCurrentCamera().getPositionX(),
            self.camerasCoordinator.getCurrentCamera().getPositionY(),
            self.camerasCoordinator.getCurrentCamera().getPositionZ()
        )
        self.sceneConstants.cameraPosition = currentCameraPosition
        
        super.performUpdate()
    }
    
    override func performRender(renderCommandEncoder: MTLRenderCommandEncoder) {
        renderCommandEncoder.pushDebugGroup("<EWS> Rendering Scene <\(self.getName())>")
        
        renderCommandEncoder.setVertexBytes(&self.sceneConstants, length: SceneConstants.stride, index: 1)
        
        self.lightsCoordinator.setLightData(renderCommandEncoder: renderCommandEncoder)
        
        super.performRender(renderCommandEncoder: renderCommandEncoder)
        
        renderCommandEncoder.popDebugGroup()
    }
    
}


extension BaseScene {
    final public func addCamera(camera: Camera, isCurrentCamera: Bool = true) {
        self.camerasCoordinator.registerCamera(camera: camera)
        
        if isCurrentCamera == true {
            self.camerasCoordinator.setCamera(type: camera.getCameraType())
        }
    }
    
    final public func updateCameras() {
        self.camerasCoordinator.update()
    }
    
    final public func addLight(lightObject: LightObject) {
        self.lightsCoordinator.addLight(lightObject: lightObject)
        
        self.addChild(child: lightObject)
    }
}

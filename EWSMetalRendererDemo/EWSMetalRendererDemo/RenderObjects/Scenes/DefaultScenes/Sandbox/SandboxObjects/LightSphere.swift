//
//  LightSphere.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 5/27/24.
//

import Foundation

class LightSphere: LightObject {
    init() {
        super.init(name: "LightSphere", meshType: MeshTypes.sphere)
        
        self.setScale(x: 0.3, y: 0.3, z: 0.3)
    }
}

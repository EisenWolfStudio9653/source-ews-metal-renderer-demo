//
//  SwiftTypesExtension.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

extension Int32: Sizeable {}

extension UInt32: Sizeable {}

extension Float: Sizeable {}

extension SIMD2<Float>: Sizeable {}

extension SIMD3<Float>: Sizeable {}

extension SIMD4<Float>: Sizeable {}

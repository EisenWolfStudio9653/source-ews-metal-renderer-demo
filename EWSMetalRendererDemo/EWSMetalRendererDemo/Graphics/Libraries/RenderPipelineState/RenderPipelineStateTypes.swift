//
//  RenderPipelineStateTypes.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

enum RenderPipelineStateTypes {
    case basic
    case instanced
    case final
    case skySphere
}

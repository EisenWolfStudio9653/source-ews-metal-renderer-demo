//
//  CursorButtonCodes.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

enum CursorButtonCodes: Int {
    case leftButton = 0
    case rightButton = 1
    case middleButton = 2
    case side1Button = 3
    case side2Button = 4
    case side3Button = 5
    
}

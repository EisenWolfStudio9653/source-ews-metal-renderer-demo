//
//  RenderPipelineStateLibrary.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class RenderPipelineStateLibrary: Library<RenderPipelineStateTypes, MTLRenderPipelineState> {
    private var libraryCache = [RenderPipelineStateTypes: RenderPipelineState]()
    
    override subscript(type: RenderPipelineStateTypes) -> MTLRenderPipelineState {
        let renderPipelineState = self.libraryCache[type]!.state!
        
        return renderPipelineState
    }
    
    override func performSetupDefaultLibrary() {
        self.libraryCache.updateValue(
            BasicRenderPipelineState(),
            forKey: RenderPipelineStateTypes.basic
        )
        
        self.libraryCache.updateValue(
            InstancedRenderPipelineState(), 
            forKey: RenderPipelineStateTypes.instanced
        )
        
        self.libraryCache.updateValue(
            FinalRenderPipelineState(),
            forKey: RenderPipelineStateTypes.final
        )
        
        self.libraryCache.updateValue(
            SkySphereRenderPipelineState(),
            forKey: RenderPipelineStateTypes.skySphere
        )
        
    }
    
}

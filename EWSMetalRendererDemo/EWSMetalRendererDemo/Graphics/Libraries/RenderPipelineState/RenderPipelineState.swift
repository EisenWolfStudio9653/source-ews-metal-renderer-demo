//
//  RenderPipelineState.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

protocol RenderPipelineState {
    var name: String { get }
    var state: MTLRenderPipelineState! { get }
}

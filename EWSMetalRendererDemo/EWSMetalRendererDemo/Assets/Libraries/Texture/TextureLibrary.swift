//
//  TextureLibrary.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class TextureLibrary: Library<TextureTypes, MTLTexture> {
    private var libraryCache = [TextureTypes: Texture]()
    
    override func performSetupDefaultLibrary() {
        
        // Metal Plate
        self.libraryCache.updateValue(
            Texture(
                textureName: "metal_plate_diff",
                textureExtension: "png",
                origin: TextureOrigin.bottomleft
            ),
            forKey: TextureTypes.diffuseMetalPlate
        )
        
        self.libraryCache.updateValue(
            Texture(
                textureName: "metal_plate_nor",
                textureExtension: "png",
                origin: TextureOrigin.bottomleft
            ),
            forKey: TextureTypes.normalMetalPlate
        )
        
        // Sky Sphere
        self.libraryCache.updateValue(
            Texture(
                textureName: "sky_sphere_clouds",
                textureExtension: "png",
                origin: TextureOrigin.bottomleft
            ),
            forKey: TextureTypes.skySphereCloud
        )
        
    }
    
    override subscript(type: TextureTypes) -> MTLTexture? {
        let metalTexture = self.libraryCache[type]?.getTexture()
        
        return metalTexture
    }
}

extension TextureLibrary {
    final public func setTexture(metalTexture: MTLTexture, type: TextureTypes) {
        self.libraryCache.updateValue(
            Texture(metalTexture: metalTexture),
            forKey: type
        )
    }
    
    final public func appendRenderPassTexture(name: String, colorAttachmentsIndex: Int, renderPassDescriptor: MTLRenderPassDescriptor, type: TextureTypes, mipmapped: Bool = false, usage: MTLTextureUsage, storageMode: MTLStorageMode = MTLStorageMode.managed) {
        
        let pixelFormat: MTLPixelFormat
        if type == TextureTypes.baseDepthRender {
            pixelFormat = EnginePreference.shared.getPixelFormat(for: PixelFormatTypes.mainDepth)
        } else {
            pixelFormat = EnginePreference.shared.getPixelFormat(for: PixelFormatTypes.mainColor)
        }
        
        let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(
            pixelFormat: pixelFormat,
            width: Int(RendererProperties.manager.getScreenSize().x),
            height: Int(RendererProperties.manager.getScreenSize().y),
            mipmapped: mipmapped
        )
        
        textureDescriptor.usage = usage
        textureDescriptor.storageMode = storageMode
        
        guard let metalTexture = MetalEngine.shared.getMetalDevice().makeTexture(descriptor: textureDescriptor) else {
            debugPrint("<EWS> TextureLibrary -> appendRenderPassTexture => Texture<\(name)> Error")
            return
        }
        
        if type == TextureTypes.baseDepthRender {
            renderPassDescriptor.depthAttachment.texture = metalTexture
        } else {
            renderPassDescriptor.colorAttachments[colorAttachmentsIndex].texture = metalTexture
            renderPassDescriptor.colorAttachments[colorAttachmentsIndex].storeAction = MTLStoreAction.store
            renderPassDescriptor.colorAttachments[colorAttachmentsIndex].loadAction = MTLLoadAction.clear
        }
        
        self.libraryCache.updateValue(
            Texture(metalTexture: metalTexture),
            forKey: type
        )
        
    }
}

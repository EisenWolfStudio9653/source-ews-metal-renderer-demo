//
//  BasicVertexDescriptor.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

struct BasicVertexDescriptor: VertexDescriptor {
    var name: String = "BasicVertexDescriptor"
    
    var descriptor: MTLVertexDescriptor!
    
    init() {
        self.descriptor = MTLVertexDescriptor()
        
        var offset = 0
        
        // Position
        self.descriptor.attributes[0].format = MTLVertexFormat.float3
        self.descriptor.attributes[0].bufferIndex = 0
        self.descriptor.attributes[0].offset = offset
        offset += SIMD3<Float>.size
        
        // Color
        self.descriptor.attributes[1].format = MTLVertexFormat.float4
        self.descriptor.attributes[1].bufferIndex = 0
        self.descriptor.attributes[1].offset = offset
        offset += SIMD4<Float>.size
        
        // Texture
        self.descriptor.attributes[2].format = MTLVertexFormat.float2
        self.descriptor.attributes[2].bufferIndex = 0
        self.descriptor.attributes[2].offset = offset
        offset += SIMD3<Float>.size // Bug caused by bit padding, use float3 instead of float2 size.
        
        // Normal
        self.descriptor.attributes[3].format = MTLVertexFormat.float3
        self.descriptor.attributes[3].bufferIndex = 0
        self.descriptor.attributes[3].offset = offset
        offset += SIMD3<Float>.size
        
        // Tangent
        self.descriptor.attributes[4].format = MTLVertexFormat.float3
        self.descriptor.attributes[4].bufferIndex = 0
        self.descriptor.attributes[4].offset = offset
        offset += SIMD3<Float>.size
        
        // Bitangent
        self.descriptor.attributes[5].format = MTLVertexFormat.float3
        self.descriptor.attributes[5].bufferIndex = 0
        self.descriptor.attributes[5].offset = offset
        offset += SIMD3<Float>.size
        
        self.descriptor.layouts[0].stride = Vertex.stride

    }
}

//
//  Material.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation
import simd

struct Material: Sizeable {
    var color: SIMD4<Float> = SIMD4<Float>(0.5, 0.5, 0.5, 1)
    var enableBaseColorTexture: Bool = false
    var enableNormalMapTexture: Bool = false
    var enableLights: Bool = true
    var shinines: Float = 2
    var ambient: SIMD3<Float> = SIMD3<Float>(repeating: 0.1)
    var diffuse: SIMD3<Float> = SIMD3<Float>(repeating: 1.0)
    var specular: SIMD3<Float> = SIMD3<Float>(repeating: 1.0)
}

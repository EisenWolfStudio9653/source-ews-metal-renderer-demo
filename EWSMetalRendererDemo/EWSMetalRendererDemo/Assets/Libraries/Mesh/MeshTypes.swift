//
//  MeshTypes.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

enum MeshTypes {
    case void
    
    // Self Defined Mesh
    case basicTriangle
    case basicSquare
    case basicCube
    
    // Common
    case sphere
    
    // Sandbox
    case model_F16
    case model_Chest
    case model_Corgi
    
    // NatureKit
    case natureGround
    case natureTentOpen
    case natureTentClosed
    case natureTreePineA
    case natureTreePineB
    case natureTreePineC
    case natureTreeOak
    
    // CastleKit
    case castleGround
    case castleGroundHill
    case castleRocksSmall
    case castleRocksLarge
    case castleTreeTrunk
    case castleTreeSmall
    case castleTreeLarge
    case castleFlag
    case castleFlagPannant
}


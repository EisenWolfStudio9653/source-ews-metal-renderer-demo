//
//  DepthStencilState.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

protocol DepthStencilState {
    var depthStencilState: MTLDepthStencilState! { get }
}

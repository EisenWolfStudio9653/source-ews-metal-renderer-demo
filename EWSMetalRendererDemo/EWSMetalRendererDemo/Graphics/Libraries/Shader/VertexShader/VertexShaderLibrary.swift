//
//  VertexShaderLibrary.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class VertexShaderLibrary: Library<VertexShaderTypes, MTLFunction> {
    private var libraryCache = [VertexShaderTypes: Shader]()
    
    override subscript(type: VertexShaderTypes) -> MTLFunction {
        let metalFunction = (self.libraryCache[type]?.getFunction())!
        
        return metalFunction
    }
    
    override func performSetupDefaultLibrary() {
        
        self.libraryCache.updateValue(
            Shader(
                name: "BasicVertexShader",
                functionName: "basic_vertex_shader"
            ),
            forKey: VertexShaderTypes.basic
        )
        
        self.libraryCache.updateValue(
            Shader(
                name: "InstancedVertexShader",
                functionName: "instanced_vertex_shader"
            ),
            forKey: VertexShaderTypes.instanced
        )
        
        self.libraryCache.updateValue(
            Shader(
                name: "FinalVertexShader",
                functionName: "final_vertex_shader"
            ),
            forKey: VertexShaderTypes.final
        )
        
        self.libraryCache.updateValue(
            Shader(
                name: "SkySphereVertexShader",
                functionName: "sky_sphere_vertex_shader"
            ),
            forKey: VertexShaderTypes.skySphere
        )
        
    }

}

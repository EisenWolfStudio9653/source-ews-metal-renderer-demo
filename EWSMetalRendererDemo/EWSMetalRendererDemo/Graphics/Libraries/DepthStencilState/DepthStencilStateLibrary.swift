//
//  DepthStencilStateLibrary.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class DepthStencilStateLibrary: Library<DepthStencilStateTypes, MTLDepthStencilState> {
    private var libraryCache = [DepthStencilStateTypes: DepthStencilState]()
    
    override subscript(type: DepthStencilStateTypes) -> MTLDepthStencilState {
        let depthStencilState = self.libraryCache[type]!.depthStencilState!
        
        return depthStencilState
    }
    
    override func performSetupDefaultLibrary() {
        self.libraryCache.updateValue(LessDepthStencilState(), forKey: DepthStencilStateTypes.less)
    }
}

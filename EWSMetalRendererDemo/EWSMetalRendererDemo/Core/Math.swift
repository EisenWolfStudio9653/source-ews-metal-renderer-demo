//
//  Math.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation
import simd

final class Math {
    private init() {}
    public static let shared = Math()
    
    public let axisX = SIMD3<Float>(1, 0, 0)
    public let axisY = SIMD3<Float>(0, 1, 0)
    public let axisZ = SIMD3<Float>(0, 0, 1)
}

extension Math {
    
}


// - TODO: Refactor
extension Float {
    var toRadians: Float {
        return (self / 180.0) * Float.pi
    }
    
    var toDegree: Float {
        return self * (180.0 / Float.pi)
    }
}

extension matrix_float4x4 {
    mutating func translate(direction: SIMD3<Float>) {
        var result = matrix_identity_float4x4
        
        let x = direction.x
        let y = direction.y
        let z = direction.z
        
        result.columns = (
            SIMD4(1, 0, 0, 0),
            SIMD4(0, 1, 0, 0),
            SIMD4(0, 0, 1, 0),
            SIMD4(x, y, z, 1)
        )
        
        self = matrix_multiply(self, result)
    }
    
    mutating func scale(axis: SIMD3<Float>) {
        var result = matrix_identity_float4x4
        
        let x = axis.x
        let y = axis.y
        let z = axis.z
        
        result.columns = (
            SIMD4(x, 0, 0, 0),
            SIMD4(0, y, 0, 0),
            SIMD4(0, 0, z, 0),
            SIMD4(0, 0, 0, 1)
        )
        
        self = matrix_multiply(self, result)
    }
    
    mutating func rotate(angle: Float, axis: SIMD3<Float>) {
        var result = matrix_identity_float4x4
        
        let x = axis.x
        let y = axis.y
        let z = axis.z
        let cosines = cos(angle)
        let sines = sin(angle)
        let minusCosines = 1 - cosines
        
        // r: row   c: column
        
        let r1c1: Float = x * x * minusCosines + cosines
        let r2c1: Float = x * y * minusCosines + z * sines
        let r3c1: Float = x * z * minusCosines - y * sines
        let r4c1: Float = 0
        
        let r1c2: Float = y * x * minusCosines - z * sines
        let r2c2: Float = y * y * minusCosines + cosines
        let r3c2: Float = y * z * minusCosines + x * sines
        let r4c2: Float = 0
        
        let r1c3: Float = z * x * minusCosines + y * sines
        let r2c3: Float = z * y * minusCosines - x * sines
        let r3c3: Float = z * z * minusCosines + cosines
        let r4c3: Float = 0
        
        let r1c4: Float = 0
        let r2c4: Float = 0
        let r3c4: Float = 0
        let r4c4: Float = 1
        
        result.columns = (
            SIMD4(r1c1, r2c1, r3c1, r4c1),
            SIMD4(r1c2, r2c2, r3c2, r4c2),
            SIMD4(r1c3, r2c3, r3c3, r4c3),
            SIMD4(r1c4, r2c4, r3c4, r4c4)
        )
        
        self = matrix_multiply(self, result)
    }
    
    mutating func perspectiveProjection(degreesFOV: Float, aspectRatio: Float, near: Float, far: Float) {
        var result = matrix_identity_float4x4
        
        let radianFOV = degreesFOV.toRadians
        
        let tangent = tan(radianFOV / 2)
        
        let x = 1.0 / (aspectRatio * tangent)
        let y = 1.0 / tangent
        let z = -((far + near) / (far - near))
        let w = -((2.0 * far * near) / (far - near))
        
        result.columns = (
            SIMD4(x, 0, 0, 0),
            SIMD4(0, y, 0, 0),
            SIMD4(0, 0, z, -1),
            SIMD4(0, 0, w, 0)
        )
        
        self = matrix_multiply(self, result)
    }
    
    mutating func orthographicProjection(left: Float, right: Float, top: Float, bottom: Float, far: Float, near: Float) {
        var result = matrix_identity_float4x4
        
        result[0][0] = 2.0 / (right - left)
        result[1][1] = 2.0 / (top - bottom)
        result[2][2] = 1.0 / (far - near)
        result[3][0] = -(top + bottom) / (top - bottom)
        result[3][2] = -(near) / (far - near)
        
        self = matrix_multiply(self, result)
    }
    
 
    
}



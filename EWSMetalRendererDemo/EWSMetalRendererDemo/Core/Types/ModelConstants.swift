//
//  ModelConstants.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation
import simd

struct ModelConstants: Sizeable {
    var modelMatrix = matrix_identity_float4x4
}

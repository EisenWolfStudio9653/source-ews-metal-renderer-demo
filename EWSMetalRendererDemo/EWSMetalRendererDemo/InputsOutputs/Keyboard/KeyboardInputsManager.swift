//
//  KeyboardInputsManager.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

final class KeyboardInputs {
    private init() {
        self.keysActuationStateContainer = [Bool](repeating: false, count: self.keyCount)
    }
    public static let manager = KeyboardInputs()
    
    private var keyCount: Int = 256
    private var keysActuationStateContainer: [Bool]
}

extension KeyboardInputs {
    public func setKeyPressed(keyCode: UInt16, isOn: Bool) {
        self.keysActuationStateContainer[Int(keyCode)] = isOn
    }
    
    public func isKeyPressed(keyCode: KeyCodes) -> Bool {
        let state = self.keysActuationStateContainer[Int(keyCode.rawValue)]
        
        return state
    }
}

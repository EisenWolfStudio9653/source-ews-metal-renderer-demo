//
//  ForestScene.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

class ForestScene: BaseScene {
    private var camera = DebugCamera()
    
    private var selectedObject: BasicObject?
    
    override func buildScene() {
        self.camera.setPosition(x: 0.0, y: 5.0, z: 10.0)
//        self.camera.setRotation(x: Float(10.0).toRadians)
        self.addCamera(camera: self.camera)
        
        // Sky Sphere
        let cloudSkySphere = SkySphere(
            name: "cloudSkySphere",
            textureType: TextureTypes.skySphereCloud
        )
        self.addChild(child: cloudSkySphere)
        
        // Lights
        let sun = LightObject(name: "Sun", meshType: MeshTypes.sphere)
        
        var sunMaterial = Material()
        sunMaterial.color = SIMD4<Float>(0.7, 0.5, 0.0, 1.0)
        sunMaterial.enableLights = false
        sun.setMaterial(material: sunMaterial)
        
        sun.setPosition(x: 0.0, y: 100.0, z: 100.0)
        sun.setScale(scaleFactor: 5.0)
        self.addLight(lightObject: sun)
        
        let light = LightObject(name: "Offset Light")
        light.setPosition(x: 0.0, y: 100.0, z: -100.0)
        light.setLightBrightness(brightness: 0.5)
        self.addLight(lightObject: light)
        
        // Terrain
        let terrain = BasicObject(
            name: "Terrain",
            meshType: MeshTypes.natureGround
        )
        
        terrain.setScale(scaleFactor: 200.0)
        self.addChild(child: terrain)
        
        // Trees
        self.addChild(child: InstancedTrees())
        
        // Tents
        let tent1 = BasicObject(
            name: "Tent1",
            meshType: MeshTypes.natureTentOpen
        )
        
        tent1.rotate(deltaY: Float(20.0).toRadians)
        self.addChild(child: tent1)
        
        
        self.selectedObject = Model_F16()
        
        self.selectedObject?.setPosition(
            x: tent1.getPositionX(),
            y: 5.0,
            z: tent1.getScaleZ() + 3.0
        )
        
        self.addChild(child: self.selectedObject!)
        
    }
    
    override func performPreUpdate() {
    
        let deltaTime = EngineTime.shared.getDeltaTime()
        let moveSpeed: Float = 3.0
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.wKey) == true {
            self.selectedObject?.move(deltaZ: -(deltaTime) * moveSpeed)
        }
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.aKey) == true {
            self.selectedObject?.move(deltaX: -(deltaTime) * moveSpeed)
        }
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.sKey) == true {
            self.selectedObject?.move(deltaZ: deltaTime * moveSpeed)
        }
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.dKey) == true {
            self.selectedObject?.move(deltaX: deltaTime * moveSpeed)
        }
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.qKey) == true {
            self.selectedObject?.rotate(deltaY: deltaTime)
        }
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.eKey) == true {
            self.selectedObject?.rotate(deltaY: -(deltaTime))
        }
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.zKey) == true {
            self.selectedObject?.move(deltaY: deltaTime * moveSpeed)
        }
        
        if KeyboardInputs.manager.isKeyPressed(keyCode: KeyCodes.xKey) == true {
            self.selectedObject?.move(deltaY: -(deltaTime) * moveSpeed)
        }
    }
}

extension ForestScene {
    
}

//
//  InstancedObject.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class InstancedObject: Node {
    private var renderPipelineStateType = RenderPipelineStateTypes.basic
    
    private var mesh: Mesh?
    
    // Texture
    private var baseColorTextureType: TextureTypes = TextureTypes.void
    private var normalMapTextureType: TextureTypes = TextureTypes.void
    
    // Material
    private var material = Material()
    
    private var nodesContainer = [Node]()
    private var modelConstantsBuffer: MTLBuffer?
    
    init(name: String, meshType: MeshTypes, instantceCount: Int) {
        debugPrint("<EWS> InstancedObject -> init => <\(name)> * \(instantceCount)")
        
        super.init(name: name)

        self.mesh = Assets.manager.getMeshLibrary()[meshType]
        self.mesh?.setInstanceCount(count: instantceCount)
        
        self.setupInstances(instanceCount: instantceCount)
        self.setupBuffers(instanceCount: instantceCount)
    }
    
    // Override Node
    override func performUpdate() {
        guard var pointer = self.modelConstantsBuffer?.contents().bindMemory(to: ModelConstants.self, capacity: self.nodesContainer.count) else {
            return
        }
        
        for node in self.nodesContainer {
            pointer.pointee.modelMatrix = matrix_multiply(self.getModelMatrix(), node.getModelMatrix())
            
            pointer = pointer.advanced(by: 1)
        }
        
        super.performUpdate()
    }

}

extension InstancedObject {
    private func setupInstances(instanceCount: Int) {
        for index in 0..<instanceCount {
            let instancedNode = Node(name: "\(self.getName())<\(index)>")
            
            self.nodesContainer.append(instancedNode)
        }
    }
    
    private func setupBuffers(instanceCount: Int) {
        self.modelConstantsBuffer = MetalEngine.shared.getMetalDevice().makeBuffer(
            length: ModelConstants.stride(instanceCount),
            options: []
        )
    }
    
}

extension InstancedObject {
    final public func setBaseColorTexture(textureType: TextureTypes) {
        self.baseColorTextureType = textureType
    }
    
    final public func setNormalMapTexture(textureType: TextureTypes) {
        self.normalMapTextureType = textureType
    }
    
    final public func setMaterial(material: Material) {
        self.material = material
    }
    
    final public func setRenderPipelineStateType(type: RenderPipelineStateTypes) {
        self.renderPipelineStateType = type
    }
    
    final public func getNodes() -> [Node] {
        return self.nodesContainer
    }
    
    final public func updateNodes(nodePositionFunction: (Node, Int) -> Void) {
        for (index, node) in self.nodesContainer.enumerated() {
            nodePositionFunction(node, index)
        }
    }
}

extension InstancedObject: Renderable {
    func executeRenderable(with renderCommandEncoder: MTLRenderCommandEncoder, shadowRenderpass: Bool) {
        renderCommandEncoder.setRenderPipelineState(Graphics.manager.getRenderPipelineStateLibrary()[RenderPipelineStateTypes.instanced])
        
        renderCommandEncoder.setDepthStencilState(Graphics.manager.getDepthStencilStateLibrary()[DepthStencilStateTypes.less])
        
        // Vertex Shader
        renderCommandEncoder.setVertexBuffer(
            self.modelConstantsBuffer,
            offset: 0,
            index: 2
        )
        
        // Fragment Shader
        renderCommandEncoder.setFragmentBytes(
            &self.material,
            length: Material.stride,
            index: 1
        )
        
        self.mesh?.drawPrimitives(renderCommandEncoder: renderCommandEncoder, shadowRenderpass: shadowRenderpass)
    }
}

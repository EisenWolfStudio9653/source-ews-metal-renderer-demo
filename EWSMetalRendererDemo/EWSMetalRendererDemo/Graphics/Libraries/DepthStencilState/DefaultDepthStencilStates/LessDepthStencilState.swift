//
//  LessDepthStencilState.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

public struct LessDepthStencilState: DepthStencilState {
    public var depthStencilState: MTLDepthStencilState!
    
    init() {
        let depthStencilDescriptor = MTLDepthStencilDescriptor()
        
        depthStencilDescriptor.isDepthWriteEnabled = true
        depthStencilDescriptor.depthCompareFunction = MTLCompareFunction.less
        
        self.depthStencilState = MetalEngine.shared.getMetalDevice().makeDepthStencilState(descriptor: depthStencilDescriptor)
    }
}



//
//  AssetsManager.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

final class Assets {
    private init() {}
    public static let manager = Assets()
    
    private var textureLibrary: TextureLibrary?
    private var meshLibrary: MeshLibrary?
}

extension Assets {
    public func initialize() {
        self.textureLibrary = TextureLibrary()
        self.meshLibrary = MeshLibrary()
    }
    
    public func getTextureLibrary() -> TextureLibrary {
        return self.textureLibrary!
    }
    
    public func getMeshLibrary() -> MeshLibrary {
        return self.meshLibrary!
    }
}

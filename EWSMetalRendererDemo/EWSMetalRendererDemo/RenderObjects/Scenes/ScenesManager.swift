//
//  ScenesManager.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation
import MetalKit

final class Scenes {
    private init() {}
    public static let manager = Scenes()
    
    private var currentScene: BaseScene?
}

extension Scenes {
    final public func setCurrentScene(type: SceneTypes) {
        switch type {
        case SceneTypes.sandbox:
            self.currentScene = SandboxScene(name: "SandboxScene")
        case SceneTypes.forest:
            self.currentScene = ForestScene(name: "ForestScene")
        default:
            debugPrint("<EWS> SceneManager -> setupCurrentScene => Unknown Case")
        }
        
    }
    
    final public func tickScene(deltaTime: Float) {
        
        guard let currentScene = self.currentScene else {
            debugPrint("<EWS> SceneManager -> tickScene => currentScene nil")
            return
        }
        
        EngineTime.shared.update(time: deltaTime)

        currentScene.updateCameras()
        
        currentScene.performUpdate()

    }
    
    final public func render(renderCommandEncoder: MTLRenderCommandEncoder, shadowRenderPass: Bool) {
        
        guard let currentScene = self.currentScene else {
            debugPrint("<EWS> SceneManager -> render => currentScene nil")
            return
        }
        
        currentScene.render(renderCommandEncoder: renderCommandEncoder, shadowRenderpass: shadowRenderPass)
    }
    
}

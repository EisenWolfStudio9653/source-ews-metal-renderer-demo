//
//  CamerasCoordinator.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

class CamerasCoordinator {
    private var camerasCache = [CameraTypes: Camera]()
    
    private var currentCamera: Camera!
}

extension CamerasCoordinator {
    public func registerCamera(camera: Camera) {
        self.camerasCache.updateValue(camera, forKey: camera.getCameraType())
    }
    
    public func getCurrentCamera() -> Camera {
        return self.currentCamera
    }
    
    public func setCamera(type cameraType: CameraTypes) {
        self.currentCamera = self.camerasCache[cameraType]
    }
    
    public func update() {
        for camera in self.camerasCache.values {
            camera.performUpdate()
        }
    }
}

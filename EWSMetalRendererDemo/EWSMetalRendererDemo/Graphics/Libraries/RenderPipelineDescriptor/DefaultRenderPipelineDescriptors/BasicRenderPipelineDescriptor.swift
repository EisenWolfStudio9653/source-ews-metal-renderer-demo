//
//  BasicRenderPipelineDescriptor.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

struct BasicRenderPipelineDescriptor: RenderPipelineDescriptor {
    var name: String = "BasicRenderPipelineDescriptor"
    
    var descriptor: MTLRenderPipelineDescriptor!
    
    init() {
        self.descriptor = MTLRenderPipelineDescriptor()
        
        self.descriptor.colorAttachments[0].pixelFormat = EnginePreference.shared.getPixelFormat(for: PixelFormatTypes.mainColor)
        
        self.descriptor.depthAttachmentPixelFormat = EnginePreference.shared.getPixelFormat(for: PixelFormatTypes.mainDepth)
        
        self.descriptor.vertexDescriptor = Graphics.manager.getVertexDescriptorLibrary()[VertexDescriptorTypes.basic]
        
        self.descriptor.vertexFunction = Graphics.manager.getVertexShaderLibrary()[VertexShaderTypes.basic]
        self.descriptor.fragmentFunction = Graphics.manager.getFragmentShaderLibrary()[FragmentShaderTypes.basic]
        
        
        
    }
}

//
//  LinearSamplerState.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

struct LinearSamplerState: SamplerState {
    var name: String = "LinearSamplerState"
    var state: MTLSamplerState!
    
    init() {
        let samplerDescriptor = MTLSamplerDescriptor()
        
        samplerDescriptor.label = self.name
        
        samplerDescriptor.minFilter = MTLSamplerMinMagFilter.linear
        samplerDescriptor.magFilter = MTLSamplerMinMagFilter.linear
        samplerDescriptor.mipFilter = MTLSamplerMipFilter.linear
        
        self.state = MetalEngine.shared.getMetalDevice().makeSamplerState(descriptor: samplerDescriptor)
    }
}

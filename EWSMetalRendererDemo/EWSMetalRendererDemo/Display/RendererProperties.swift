//
//  RendererProperties.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

final class RendererProperties {
    private init() {}
    public static let manager = RendererProperties()
    
    private var screenSize = SIMD2<Float>(repeating: 0.0)
    
    private var aspectRatio: Float = 1.0
    
}

extension RendererProperties {
    public func setScreenSize(screenSize: SIMD2<Float>) {
        self.screenSize = screenSize
        
        self.aspectRatio = self.screenSize.x / self.screenSize.y
    }
    
    public func getScreenSize() -> SIMD2<Float> {
        return self.screenSize
    }
    
    public func getAspectRatio() -> Float {
        return self.aspectRatio
    }
}

//
//  Mesh.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class Mesh {
    private var instanceCount: Int = 1
    private var verticesContainer = [Vertex]()
    private var vertexCount: Int = 0
    private var vertexBuffer: MTLBuffer? = nil
    private var submeshesContainer = [Submesh]()
    
    // Model Mesh
    init(modelName: String, fileExtension: String = "obj") {
        self.verticesContainer.removeAll()
        self.submeshesContainer.removeAll()
        
        guard let resourceURL = Bundle.main.url(forResource: modelName, withExtension: fileExtension) else {
            debugPrint("<EWS> Mesh -> init ModelMesh => Model URL Error")
            return
        }
        
        let metalKitModelIOVertexDescriptor = MTKModelIOVertexDescriptorFromMetal(Graphics.manager.getVertexDescriptorLibrary()[VertexDescriptorTypes.basic])
        
        guard let vertextPosition = metalKitModelIOVertexDescriptor.attributes[0] as? MDLVertexAttribute,
                let vertexColor = metalKitModelIOVertexDescriptor.attributes[1] as? MDLVertexAttribute,
                let textureCoordinate = metalKitModelIOVertexDescriptor.attributes[2] as? MDLVertexAttribute,
                let normal = metalKitModelIOVertexDescriptor.attributes[3] as? MDLVertexAttribute,
                let tangent = metalKitModelIOVertexDescriptor.attributes[4] as? MDLVertexAttribute,
                let bitangent = metalKitModelIOVertexDescriptor.attributes[5] as? MDLVertexAttribute
        else {
            debugPrint("<EWS> Mesh -> init ModelMesh => ModelIO VertexDescriptor Error")
            return
        }
        
        vertextPosition.name = MDLVertexAttributePosition
        vertexColor.name = MDLVertexAttributeColor
        textureCoordinate.name = MDLVertexAttributeTextureCoordinate
        normal.name = MDLVertexAttributeNormal
        tangent.name = MDLVertexAttributeTangent
        bitangent.name = MDLVertexAttributeBitangent
        
        let bufferAllocator = MTKMeshBufferAllocator(device: MetalEngine.shared.getMetalDevice())
        
        let asset = MDLAsset(
            url: resourceURL,
            vertexDescriptor: metalKitModelIOVertexDescriptor,
            bufferAllocator: bufferAllocator,
            preserveTopology: true,
            error: nil
        )
     
        asset.loadTextures()
        
        // Mesh material, MDLMesh (ModelIO)
        var modelMeshesContainer = [MDLMesh]()
        
        do {
            modelMeshesContainer = try MTKMesh.newMeshes(
                asset: asset,
                device: MetalEngine.shared.getMetalDevice()
            ).modelIOMeshes
        } catch {
            debugPrint("<EWS> Mesh -> init ModelMesh => MDLMesh Error")
        }
        
        // Mesh positioning, MTKMesh
        var metalKitMeshesContainer = [MTKMesh]()
        
        for modelMesh in modelMeshesContainer {
            modelMesh.addTangentBasis(
                forTextureCoordinateAttributeNamed: MDLVertexAttributeTextureCoordinate,
                tangentAttributeNamed: MDLVertexAttributeTangent,
                bitangentAttributeNamed: MDLVertexAttributeBitangent
            )
            
            modelMesh.vertexDescriptor = metalKitModelIOVertexDescriptor
            
            do {
                let metalKitMesh = try MTKMesh(
                    mesh: modelMesh,
                    device: MetalEngine.shared.getMetalDevice()
                )
                
                metalKitMeshesContainer.append(metalKitMesh)
            } catch {
                debugPrint("<EWS> Mesh -> init ModelMesh => MTKMesh Error")
            }
        }
        
        let metalKitMesh = metalKitMeshesContainer[0]
        let modelMesh = modelMeshesContainer[0]
        
        self.vertexBuffer = metalKitMesh.vertexBuffers[0].buffer
        self.vertexCount = metalKitMesh.vertexCount
        
        for submeshIndex in 0..<metalKitMesh.submeshes.count {
            
            
            guard let modelSubmesh = modelMesh.submeshes?[submeshIndex] as? MDLSubmesh else {
                debugPrint("<EWS> Mesh -> init ModelMesh => MDLSubmesh Error")
                return
            }
            
            let submesh = Submesh(
                metalKitSubmesh: metalKitMesh.submeshes[submeshIndex],
                modelSubmesh: modelSubmesh
            )
            
            self.submeshesContainer.append(submesh)
            
        }
    }
    
    // Custom Mesh
    init() {
        self.verticesContainer.removeAll()
        self.submeshesContainer.removeAll()
        
        self.setupMesh()
        
        // Setup buffer
        if self.verticesContainer.count > 0 {
            self.vertexBuffer = MetalEngine.shared.getMetalDevice().makeBuffer(
                bytes: self.verticesContainer,
                length: Vertex.stride(self.verticesContainer.count),
                options: []
            )
        }
        
        
    }
    
    // Base functions for overriding
    public func setupMesh() {}
}



extension Mesh {
    final public func setInstanceCount(count: Int) {
        self.instanceCount = count
    }
    
    final public func addVertex(position: SIMD3<Float>, color: SIMD4<Float>, textureCoordinate: SIMD2<Float> = SIMD2<Float>(repeating: 0.0), normal: SIMD3<Float> = SIMD3<Float>(0, 1, 0), tangent: SIMD3<Float> = SIMD3<Float>(1, 0, 0), bitangent: SIMD3<Float> = SIMD3<Float>(0, 0, 1)) {
        
        self.verticesContainer.append(
            Vertex(
                position: position,
                color: color,
                textureCoordinate: textureCoordinate,
                normal: normal,
                tangent: tangent,
                bitangent: bitangent
            )
        )
        
        self.vertexCount += 1
    }
    
    final public func addSubmesh(submesh: Submesh) {
        self.submeshesContainer.append(submesh)
    }
    
    final public func drawPrimitives(renderCommandEncoder: MTLRenderCommandEncoder, baseColorTextureType: TextureTypes = TextureTypes.void, normalMapTextureType: TextureTypes = TextureTypes.void, material: Material? = nil, shadowRenderpass: Bool = false) {
        
        guard self.vertexBuffer != nil else {
//            debugPrint("<EWS> Mesh -> drawPrimitives => vertexBuffer nil")
            return
        }
        
        renderCommandEncoder.setVertexBuffer(
            self.vertexBuffer,
            offset: 0,
            index: 0
        )
        
        if self.submeshesContainer.count > 0 {
            // Indexed vertices (ModelMesh or CustomMesh with indices)
            for submesh in self.submeshesContainer {
                
                guard let submeshIndexBuffer = submesh.getIndexBuffer() else {
                    return
                }
                
                submesh.applyTexture(
                    renderCommandEncoder: renderCommandEncoder,
                    baseColorTextureType: baseColorTextureType,
                    normalMapTextureType: normalMapTextureType
                )
                
                submesh.applyMaterial(
                    renderCommandEncoder: renderCommandEncoder,
                    material: material
                )
                
                if shadowRenderpass == true {
                    
                }
                
                renderCommandEncoder.drawIndexedPrimitives(
                    type: submesh.getPrimitiveType(),
                    indexCount: submesh.getIndexCount(),
                    indexType: submesh.getIndexType(),
                    indexBuffer: submeshIndexBuffer,
                    indexBufferOffset: submesh.getIndexBufferOffset(),
                    instanceCount: self.instanceCount
                )
                
            }
            
        } else {
            // Unindexed vertices (CustomMesh without indices)
            renderCommandEncoder.drawPrimitives(
                type: MTLPrimitiveType.triangle,
                vertexStart: 0,
                vertexCount: self.vertexCount,
                instanceCount: self.instanceCount
            )
            
        }
        
    }
    
}






class EmptyMesh: Mesh {}

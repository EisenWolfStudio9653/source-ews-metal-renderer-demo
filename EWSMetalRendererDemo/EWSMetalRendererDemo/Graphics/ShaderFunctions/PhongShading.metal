//
//  PhongShading.metal
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

#include <metal_stdlib>
#include "SharedStructures.metal"
using namespace metal;

float3 calculatePhongShadingIntensity(constant Material &material,
                                      constant LightData *lightDatas,
                                      int lightCount,
                                      float3 worldPosition,
                                      float3 unitNormal,
                                      float3 unitToCameraVector) {
    
    return float3(0, 0, 0);
}


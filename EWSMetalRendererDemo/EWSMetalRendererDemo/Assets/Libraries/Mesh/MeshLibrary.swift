//
//  MeshLibrary.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class MeshLibrary: Library<MeshTypes, Mesh> {
    private var libraryCache = [MeshTypes: Mesh]()
    
    override subscript(type: MeshTypes) -> Mesh {
        let mesh = self.libraryCache[type]!
        
        return mesh
    }
    
    override func performSetupDefaultLibrary() {
        
        self.libraryCache.updateValue(
            EmptyMesh(),
            forKey: MeshTypes.void
        )
        
        self.libraryCache.updateValue(
            BasicTriangleMesh(),
            forKey: MeshTypes.basicTriangle
        )
      
        self.libraryCache.updateValue(
            BasicSquareMesh(),
            forKey: MeshTypes.basicSquare
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "sphere"),
            forKey: MeshTypes.sphere
        )
        
        // NatureKit
        self.libraryCache.updateValue(
            Mesh(modelName: "ground_grass"),
            forKey: MeshTypes.natureGround
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "tent_detailedOpen"),
            forKey: MeshTypes.natureTentOpen
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "tent_detailedClosed"),
            forKey: MeshTypes.natureTentClosed
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "tree_pineTallA"),
            forKey: MeshTypes.natureTreePineA
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "tree_pineTallB"),
            forKey: MeshTypes.natureTreePineB
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "tree_pineTallC"),
            forKey: MeshTypes.natureTreePineC
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "tree_oak"),
            forKey: MeshTypes.natureTreeOak
        )
        
        // CastleKit
        self.libraryCache.updateValue(
            Mesh(modelName: "ground"),
            forKey: MeshTypes.castleGround
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "ground-hills"),
            forKey: MeshTypes.castleGroundHill
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "rocks-small"),
            forKey: MeshTypes.castleRocksSmall
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "rocks-large"),
            forKey: MeshTypes.castleRocksLarge
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "tree-trunk"),
            forKey: MeshTypes.castleTreeTrunk
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "tree-small"),
            forKey: MeshTypes.castleTreeSmall
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "tree-large"),
            forKey: MeshTypes.castleTreeLarge
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "flag"),
            forKey: MeshTypes.castleFlag
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "flag-pennant"),
            forKey: MeshTypes.castleFlagPannant
        )
        
        // Sandbox
        self.libraryCache.updateValue(
            Mesh(modelName: "f16"),
            forKey: MeshTypes.model_F16
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "chest"),
            forKey: MeshTypes.model_Chest
        )
        
        self.libraryCache.updateValue(
            Mesh(modelName: "13467_Cardigan_Welsh_Corgi_v1_L3"),
            forKey: MeshTypes.model_Corgi
        )
        
    }
    
}

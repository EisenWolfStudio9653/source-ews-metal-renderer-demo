//
//  SandboxScene.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

class SandboxScene: BaseScene {
    private var debugCamera = DebugCamera()
    
//    private var cubeCollection1: InstancedCubeCollection?
//    private var cubeCollection2: InstancedCubeCollection?
//    private var cubeCollection3: InstancedCubeCollection?
//    private var cubeCollection4: InstancedCubeCollection?
    

    private var leftLightSource = LightSphere()
    private var middleLightSource = LightSphere()
    private var rightLightSource = LightSphere()
    
    // Override BaseScene
    override func buildScene() {
        
        self.debugCamera.setPosition(x: 0.0, y: 0.0, z: 10.0)
        self.addCamera(camera: self.debugCamera, isCurrentCamera: true)
        
        self.leftLightSource.setPosition(x: -1.0, y: 1.0, z: 0.0)
 
        var leftLightSourceMaterial = Material()
        leftLightSourceMaterial.color = SIMD4<Float>(1.0, 0.0, 0.0, 1.0)
        leftLightSourceMaterial.enableLights = false
        
        self.leftLightSource.setMaterial(material: leftLightSourceMaterial)
        self.leftLightSource.setLightColor(r: 1.0, g: 0.0, b: 0.0)
        self.leftLightSource.setLightBrightness(brightness: 0.95)
        self.leftLightSource.setLightAmbientIntensity(intensity: 0.05)
        self.addLight(lightObject: self.leftLightSource)
        

        var rightLightSourceMaterial = Material()
        rightLightSourceMaterial.color = SIMD4<Float>(0.0, 1.0, 0.0, 1.0)
        rightLightSourceMaterial.enableLights = false
        
        self.rightLightSource.setMaterial(material: rightLightSourceMaterial)
        self.rightLightSource.setPosition(x: 1.0, y: 1.0, z: 0.0)
        self.rightLightSource.setLightColor(r: 0.0, g: 1.0, b: 0.0)
        self.rightLightSource.setLightBrightness(brightness: 0.95)
        self.rightLightSource.setLightAmbientIntensity(intensity: 0.05)
        self.addLight(lightObject: self.rightLightSource)
      
        
//        let object = Model_F16()
        let object = Model_Chest()
        
        object.setPosition(y: -0.75)
//        self.object.setBaseColorTexture(textureType: TextureTypes.diffuseMetalPlate)
//        self.object.setNormalMapTexture(textureType: TextureTypes.normalMetalPlate)
        
        self.addChild(child: object)
  
    }

    // Override Node
    override func performPreUpdate() {
        
        let totalTime = EngineTime.shared.getTotalTime()
        
        self.leftLightSource.setPosition(x: cos(totalTime) - 1)
        self.middleLightSource.setPosition(x: cos(totalTime))
        self.rightLightSource.setPosition(x: cos(totalTime) + 1)
        
    }
    
}

extension SandboxScene {
    private func setupCursorTracjingTriangleArray() {
        let value = 3
        
        for y in -value..<value {
            for x in -value..<value {
                let object = CursorTrackingTriangle(camera: self.debugCamera)
                
                object.setPosition(x: Float(Float(x) + 0.5) / (Float(value) * 0.1))
                object.setPosition(y: Float(Float(y) + 0.5) / (Float(value) * 0.1))
                
                object.setScale(scaleFactor: 0.1)
                
                self.addChild(child: object)
            }
        }
    }
    
    private func setup3DCubesArray() {
        let value = 10
        
        for y in -value..<value {
            let positionY = Float(y) + 0.5
            
            for x in -value..<value {
                let positionX = Float(x) + 0.5
                
                for z in -value..<value {
                    let positionZ = Float(z) + 0.5
                    
                    let object = CubeObject(camera: self.debugCamera)
                    
                    object.setPosition(
                        x: positionX / (Float(value) * 1),
                        y: positionY / (Float(value) * 1),
                        z: positionZ / (Float(value) * 1)
                    )
                    
                    
                    object.setScale(scaleFactor: 0.3)
                    
                    self.addChild(child: object)
                }
            }
        }
        
    }
}

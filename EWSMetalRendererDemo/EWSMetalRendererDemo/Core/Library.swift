//
//  Library.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

class Library<Types, Value> {
    init() {
        self.performSetupDefaultLibrary()
    }
    
    subscript(_ type: Types) -> Value? {
        return nil
    }
    
    public func performSetupDefaultLibrary() {
        
    }
}

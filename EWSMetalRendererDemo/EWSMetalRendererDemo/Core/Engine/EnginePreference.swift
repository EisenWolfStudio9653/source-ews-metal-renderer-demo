//
//  EnginePreference.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

final class EnginePreference {
    private init() {}
    public static let shared = EnginePreference()

}

extension EnginePreference {
    public func getClearColor() -> MTLClearColor {
        return ClearColors.darkGrey
    }
    
    public func getPixelFormat(for type: PixelFormatTypes) -> MTLPixelFormat {
        switch type {
        case PixelFormatTypes.mainColor:
            return MTLPixelFormat.bgra8Unorm_srgb
        case PixelFormatTypes.mainDepth:
            return MTLPixelFormat.depth32Float
        case PixelFormatTypes.customDepth:
            return MTLPixelFormat.depth16Unorm
        }
    }

}

enum PixelFormatTypes {
    case mainColor
    case mainDepth
    case customDepth
}

enum ClearColors {
    static let white = MTLClearColor(
        red: 1.0,
        green: 1.0,
        blue: 1.0,
        alpha: 1.0
    )
    
    static let darkGrey = MTLClearColor(
        red: 0.01,
        green: 0.01,
        blue: 0.01,
        alpha: 1.0
    )
    
    static let green = MTLClearColor(
        red: 0.25,
        green: 0.50,
        blue: 0.25,
        alpha: 1.0
    )
    
    static let grey = MTLClearColor(
        red: 0.50,
        green: 0.50,
        blue: 0.50,
        alpha: 1.0
    )
}

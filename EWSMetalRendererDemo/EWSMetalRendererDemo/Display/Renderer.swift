//
//  Renderer.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class Renderer: NSObject {
    private var baseRenderPassDescriptor = MTLRenderPassDescriptor()
    private var shadowRenderPassDescriptor = MTLRenderPassDescriptor()
    
    init(metalView: MTKView) {
        super.init()
        
        self.updateScreenSize(view: metalView)
        
        Scenes.manager.setCurrentScene(type: SceneTypes.forest)
        
        self.setupBaseRenderPassDescriptor()
    }
}

extension Renderer {
    private func updateScreenSize(view: MTKView) {
        let screenWidth = Float(view.bounds.size.width)
        let screenHeight = Float(view.bounds.size.height)
        
        RendererProperties.manager.setScreenSize(screenSize: SIMD2<Float>(screenWidth, screenHeight))
    }
    
    private func setupBaseRenderPassDescriptor() {
        // Render targer (Texture): Base color texture
        Assets.manager.getTextureLibrary().appendRenderPassTexture(
            name: "BaseColor",
            colorAttachmentsIndex: 0,
            renderPassDescriptor: self.baseRenderPassDescriptor,
            type: TextureTypes.baseColorRender,
            usage: [
                MTLTextureUsage.renderTarget,
                MTLTextureUsage.shaderRead
            ]
        )
        
        // Render targer (Texture): Custom
        Assets.manager.getTextureLibrary().appendRenderPassTexture(
            name: "customColor1",
            colorAttachmentsIndex: 1,
            renderPassDescriptor: self.baseRenderPassDescriptor,
            type: TextureTypes.customColor1Render,
            usage: [
                MTLTextureUsage.renderTarget,
                MTLTextureUsage.shaderRead
            ]
        )
            
        Assets.manager.getTextureLibrary().appendRenderPassTexture(
            name: "customColor2",
            colorAttachmentsIndex: 2,
            renderPassDescriptor: self.baseRenderPassDescriptor,
            type: TextureTypes.customColor2Render,
            usage: [
                MTLTextureUsage.renderTarget,
                MTLTextureUsage.shaderRead
            ]
        )
 
        // Render targer (Texture): Base depth texture
        Assets.manager.getTextureLibrary().appendRenderPassTexture(
            name: "BaseDepth",
            colorAttachmentsIndex: 0, // Don't care for the depth texture
            renderPassDescriptor: self.baseRenderPassDescriptor,
            type: TextureTypes.baseDepthRender,
            usage: [
                MTLTextureUsage.renderTarget
            ],
            storageMode: MTLStorageMode.private // Access only by the GPU.
        )
    }
    
//    private func setupShadowRenderPassDescriptor() {
//        // Render targer (Texture): Base color texture
//        Assets.manager.getTextureLibrary().appendRenderPassTexture(
//            name: "BaseColor",
//            colorAttachmentsIndex: 0,
//            renderPassDescriptor: self.shadowRenderPassDescriptor,
//            type: TextureTypes.baseColorRender,
//            usage: [
//                MTLTextureUsage.renderTarget,
//                MTLTextureUsage.shaderRead
//            ]
//        )
//        
//        // Render targer (Texture): Custom
//        Assets.manager.getTextureLibrary().appendRenderPassTexture(
//            name: "customColor1",
//            colorAttachmentsIndex: 1,
//            renderPassDescriptor: self.shadowRenderPassDescriptor,
//            type: TextureTypes.customColor1Render,
//            usage: [
//                MTLTextureUsage.renderTarget,
//                MTLTextureUsage.shaderRead
//            ]
//        )
//            
//        Assets.manager.getTextureLibrary().appendRenderPassTexture(
//            name: "customColor2",
//            colorAttachmentsIndex: 2,
//            renderPassDescriptor: self.shadowRenderPassDescriptor,
//            type: TextureTypes.customColor2Render,
//            usage: [
//                MTLTextureUsage.renderTarget,
//                MTLTextureUsage.shaderRead
//            ]
//        )
// 
//        // Render targer (Texture): Base depth texture
//        Assets.manager.getTextureLibrary().appendRenderPassTexture(
//            name: "BaseDepth",
//            colorAttachmentsIndex: 0, // Don't care for the depth texture
//            renderPassDescriptor: self.shadowRenderPassDescriptor,
//            type: TextureTypes.baseDepthRender,
//            usage: [
//                MTLTextureUsage.renderTarget
//            ],
//            storageMode: MTLStorageMode.private // Access only by the GPU.
//        )
//    }
    
    private func baseRenderPass(commandBuffer: MTLCommandBuffer) {
        
        guard let renderCommandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: self.baseRenderPassDescriptor) else {
            debugPrint("<EWS> Renderer -> baseRenderPass => renderCommandEncoder nil")
            return
        }
        renderCommandEncoder.label = "<Base> RenderCommandEncoder"
        
        /* ======================== GPU DebugGroup ==========================*/
        renderCommandEncoder.pushDebugGroup("<EWS> Renderer -> BaseRender Started")
        
        Scenes.manager.render(renderCommandEncoder: renderCommandEncoder, shadowRenderPass: false)
        
        renderCommandEncoder.popDebugGroup()
        /* ======================== GPU DebugGroup ==========================*/
        
        renderCommandEncoder.endEncoding()
    }
    
//    private func shadowRenderPass(commandBuffer: MTLCommandBuffer) {
//        
//        guard let renderCommandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: self.shadowRenderPassDescriptor) else {
//            debugPrint("<EWS> Renderer -> shadowRenderPass => renderCommandEncoder nil")
//            return
//        }
//        renderCommandEncoder.label = "<Shadow> RenderCommandEncoder"
//        
//        /* ======================== GPU DebugGroup ==========================*/
//        renderCommandEncoder.pushDebugGroup("<EWS> Renderer -> ShadowRender Started")
//        
//        Scenes.manager.render(renderCommandEncoder: renderCommandEncoder, shadowRenderPass: true)
//        
//        renderCommandEncoder.popDebugGroup()
//        /* ======================== GPU DebugGroup ==========================*/
//        
//        renderCommandEncoder.endEncoding()
//    }
    
    private func finalRenderPass(commandBuffer: MTLCommandBuffer, view: MTKView) {
        
        guard let finalRenderPassDescriptor = view.currentRenderPassDescriptor else {
            debugPrint("<EWS> Renderer -> finalRenderPass => finalRenderPassDescriptor nil")
            return
        }
        
        guard let renderCommandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: finalRenderPassDescriptor) else {
            debugPrint("<EWS> Renderer -> finalRenderPass => renderCommandEncoder nil")
            return
        }
        renderCommandEncoder.label = "<Final> RenderCommandEncoder"
        
        /* ======================== GPU DebugGroup ==========================*/
        renderCommandEncoder.pushDebugGroup("<EWS> Renderer -> Final Render Started")
        
        renderCommandEncoder.setRenderPipelineState(Graphics.manager.getRenderPipelineStateLibrary()[RenderPipelineStateTypes.final])
        
        renderCommandEncoder.setFragmentTexture(
            Assets.manager.getTextureLibrary()[TextureTypes.baseColorRender],
            index: 0
        )
        renderCommandEncoder.setFragmentSamplerState(
            Graphics.manager.getSamplerStateLibrary()[SamplerStateTypes.linear],
            index: 0
        )
        let displayPanel = Assets.manager.getMeshLibrary()[MeshTypes.basicSquare]
        displayPanel.drawPrimitives(renderCommandEncoder: renderCommandEncoder)
    
        renderCommandEncoder.popDebugGroup()
        /* ======================== GPU DebugGroup ==========================*/
        
        renderCommandEncoder.endEncoding()
    }
    
}

extension Renderer: MTKViewDelegate {
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        self.updateScreenSize(view: view)
    }
    
    func draw(in view: MTKView) {
        let renderFPS = Float(view.preferredFramesPerSecond)
        
        Scenes.manager.tickScene(deltaTime: 1.0 / renderFPS)
        
        guard let commandBuffer = MetalEngine.shared.getMetalCommandQueue().makeCommandBuffer() else {
            debugPrint("<EWS> Renderer -> draw => commandBuffer nil")
            return
        }
        commandBuffer.label = "<Renderer>CommandBuffer"

        self.baseRenderPass(commandBuffer: commandBuffer)
        self.finalRenderPass(commandBuffer: commandBuffer, view: view)
        
        // Copy texture within the GPU
//        let blitCommandEncoder = commandBuffer?.makeBlitCommandEncoder()

        guard let currentDrawable = view.currentDrawable else {
            debugPrint("<EWS> Renderer -> draw => currentDrawable nil")
            return
        }
        commandBuffer.present(currentDrawable)
        
        commandBuffer.commit()
    }
    
}


//
//  VertexDescriptorLibrary.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class VertexDescriptorLibrary: Library<VertexDescriptorTypes, MTLVertexDescriptor> {
    private var libraryCache = [VertexDescriptorTypes: VertexDescriptor]()
    
    override subscript(type: VertexDescriptorTypes) -> MTLVertexDescriptor {
        let metalVertexDescriptor = self.libraryCache[type]!.descriptor!
        
        return metalVertexDescriptor
    }
    
    override func performSetupDefaultLibrary() {
        self.libraryCache.updateValue(BasicVertexDescriptor(), forKey: VertexDescriptorTypes.basic)
    }
    
}

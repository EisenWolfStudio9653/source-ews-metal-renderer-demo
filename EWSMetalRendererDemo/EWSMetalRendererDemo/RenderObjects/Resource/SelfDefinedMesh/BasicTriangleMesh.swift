//
//  BasicTriangleMesh.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

class BasicTriangleMesh: Mesh {
    override func setupMesh() {
        
        // Top Middle
        self.addVertex(
            position: SIMD3<Float>(0.0, 1.0, 0.0),
            color: SIMD4<Float>(1, 0, 0, 1),
            textureCoordinate: SIMD2<Float>(0.5, 1.0)
        )
        
        // Bottom Left
        self.addVertex(
            position: SIMD3<Float>(-1.0, -1.0, 0.0),
            color: SIMD4<Float>(0, 1, 0, 1),
            textureCoordinate: SIMD2<Float>(0.0, 0.0)
        )
        
        // Bottom Right
        self.addVertex(
            position: SIMD3<Float>(1.0, -1.0, 0.0),
            color: SIMD4<Float>(0, 0, 1, 1),
            textureCoordinate: SIMD2<Float>(1.0, 0.0)
        )
        
    }
}

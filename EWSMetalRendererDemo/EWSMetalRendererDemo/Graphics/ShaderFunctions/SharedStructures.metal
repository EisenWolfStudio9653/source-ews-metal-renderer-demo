//
//  SharedStructures.metal
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

#ifndef SHAREDSTRUCTURES_METAL
#define SHAREDSTRUCTURES_METAL

#include <metal_stdlib>
using namespace metal;

struct VertexIn {
    float3 postion [[ attribute(0) ]];
    float4 color [[ attribute(1) ]];
    float2 textureCoordinate [[ attribute(2) ]];
    float3 normal [[ attribute(3) ]];
    float3 tangent [[ attribute(4) ]];
    float3 bitangent [[ attribute(5) ]];
};

struct FragmentOutput {
    half4 color0 [[ color(0) ]];
    half4 color1 [[ color(1) ]];
    half4 color2 [[ color(2) ]];
};

struct RasterizerData {
    float totalEngineTime;
    
    float4 position [[ position ]];
    float4 color;
    float2 textureCoordinate;
    
    float3 worldPosition;
    float3 cameraVector;
    
    float3 surfaceNormal;
    float3 surfaceTangent;
    float3 surfaceBitangent;
};

struct FinalRasterizerData {
    float4 finalPosition [[ position ]];
    float2 finalTextureCoordinate;
};

struct ModelConstants {
    float4x4 modelMatrix;
};

struct SceneConstants {
    float totalEngineTime;
    float4x4 skySphereViewMatrix;
    float4x4 viewMatrix;
    float4x4 projectionMatrix;
    float3 cameraPosition;
};

struct Material {
    float4 color;
    bool enableBaseColorTexture;
    bool enableNormalMapTexture;
    bool enableLights;
    float shinines;
    float3 ambient;
    float3 diffuse;
    float3 specular;
};

struct LightData {
    float3 position;
    float3 color;
    float brightness;
    
    float ambientIntensity;
    float diffuseIntensity;
    float specularIntensity;
};

#endif

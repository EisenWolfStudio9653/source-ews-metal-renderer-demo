//
//  RendererView.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import MetalKit

class RendererView: MTKView {
    private var renderer: Renderer?
    
    init(frame frameRect: CGRect) {
        guard let device = MTLCreateSystemDefaultDevice() else {
            fatalError("<EWS> GPU Not Available")
        }
        
        super.init(frame: frameRect, device: device)
        
        self.device = device
        
        MetalEngine.shared.ignite(metalDevice: device)
        
        self.renderer = Renderer(metalView: self)
        
        self.clearColor = EnginePreference.shared.getClearColor()
        self.colorPixelFormat = EnginePreference.shared.getPixelFormat(for: PixelFormatTypes.mainColor)
    
        self.framebufferOnly = false
        
        self.delegate = self.renderer
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension RendererView {
    override var acceptsFirstResponder: Bool {
        return true
    }
    
    // - MARK: Keyboard Inputs
    override func keyDown(with event: NSEvent) {
        KeyboardInputs.manager.setKeyPressed(keyCode: event.keyCode, isOn: true)
    }
    
    override func keyUp(with event: NSEvent) {
        KeyboardInputs.manager.setKeyPressed(keyCode: event.keyCode, isOn: false)
    }
    
}

// - MARK: Cursor Inputs
extension RendererView {
    // Left Button
    override func mouseDown(with event: NSEvent) {
        CursorInputs.manager.setMouseButtonPressed(mouseButton: event.buttonNumber, isOn: true)
    }
    
    override func mouseUp(with event: NSEvent) {
        CursorInputs.manager.setMouseButtonPressed(mouseButton: event.buttonNumber, isOn: false)
    }
    
    // Right Button
    override func rightMouseDown(with event: NSEvent) {
        CursorInputs.manager.setMouseButtonPressed(mouseButton: event.buttonNumber, isOn: true)
    }
    
    override func rightMouseUp(with event: NSEvent) {
        CursorInputs.manager.setMouseButtonPressed(mouseButton: event.buttonNumber, isOn: false)
    }
    
    // Additional Buttons
    override func otherMouseDown(with event: NSEvent) {
        CursorInputs.manager.setMouseButtonPressed(mouseButton: event.buttonNumber, isOn: true)
    }
    
    override func otherMouseUp(with event: NSEvent) {
        CursorInputs.manager.setMouseButtonPressed(mouseButton: event.buttonNumber, isOn: false)
    }
    
    // Scroll
    override func scrollWheel(with event: NSEvent) {
        CursorInputs.manager.setVerticalScrollPosition(deltaY: Float(event.deltaY))
    }
    
    // Cursor Movement
    override func updateTrackingAreas() {
        let trackingArea = NSTrackingArea(
            rect: self.bounds,
            options: [
                NSTrackingArea.Options.activeAlways,
                NSTrackingArea.Options.mouseMoved,
                NSTrackingArea.Options.enabledDuringMouseDrag
            ],
            owner: self,
            userInfo: nil
        )
        
        self.addTrackingArea(trackingArea)
    }
    
    override func mouseMoved(with event: NSEvent) {
        self.updateCursorPositionChanged(event: event)
    }
    
    override func mouseDragged(with event: NSEvent) {
        self.updateCursorPositionChanged(event: event)
    }
    
    override func rightMouseDragged(with event: NSEvent) {
        self.updateCursorPositionChanged(event: event)
    }
    
    override func otherMouseDragged(with event: NSEvent) {
        self.updateCursorPositionChanged(event: event)
    }
    
}

extension RendererView {
    private func updateCursorPositionChanged(event: NSEvent) {
        let overallX = Float(event.locationInWindow.x)
        let overallY = Float(event.locationInWindow.y)
        
        let deltaX = Float(event.deltaX)
        let deltaY = Float(event.deltaY)
        
        CursorInputs.manager.setCursorPositionChange(
            overallPosition: SIMD2<Float>(overallX, overallY),
            deltaPosition:  SIMD2<Float>(deltaX, deltaY)
        )
    }
    
}


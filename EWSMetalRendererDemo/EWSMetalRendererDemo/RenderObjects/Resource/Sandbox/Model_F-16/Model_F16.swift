//
//  Model_F16.swift
//  EWSMetalRendererDemo
//
//  Created by EWS EisenWolfStudio on 12/25/23.
//

import Foundation

class Model_F16: BasicObject {
    init() {
        super.init(name: "Model_F-16", meshType: MeshTypes.model_F16)
    }
}
